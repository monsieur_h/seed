------------------------------------------------------------------------
-- @file menu_gamestates.lua
-- File storing the differents menu gamestates and their callbacks
------------------------------------------------------------------------

-- Loading sounds for the buttons
clicksound              =   love.audio.newSource("res/sounds/button_onClick.wav", "static")
hoversound              =   love.audio.newSource("res/sounds/button_onHover.wav", "static")
mainmenu_music          =   love.audio.newSource("res/music/mainmenu.ogg")
mainmenu_music:setLooping(true)

-- States: SEEDING, MENU, SURFACE, GROUND, PAUSE
require "gamestates.splashscreen"
require "gamestates.mainmenu"
require "gamestates.optionmenu"
require "gamestates.newgame"
require "gamestates.graphic_option"
require "gamestates.sound_option"
require "gamestates.stats_menu"
require "gamestates.achievements_menu"
require "gamestates.species_detail"


TRANSITION              =   {}
TRANSITION.ONGOING      =   false
TRANSITION.TIME         =   0.6
TRANSITION.ELAPSED      =   0
TRANSITION.SOUND_IN     =   love.audio.newSource("res/sounds/transition_in.ogg", "static")
TRANSITION.SOUND_OUT    =   love.audio.newSource("res/sounds/transition_out.ogg", "static")
TRANSITION.speed        =   nil
TRANSITION.direction    =   nil
TRANSITION.zoomspeed    =   nil
TRANSITION.rotspeed     =   nil
TRANSITION.nextGamestate=   nil
TRANSITION.cam          =   nil

function TRANSITION:goTo(p_gamestate)
    assert(p_gamestate, "Can't transition to nil gamestate")
    self.ONGOING        =   true
    self.nextGamestate  =   p_gamestate
end

function TRANSITION:update(dt)
    if self.ONGOING then
        self.ELAPSED = self.ELAPSED+dt
        if not self.direction then
        print("Camera transition")
        --~ print(self.nextGamestate.position, vector(self.cam:getPos()) )
            local len_vector     =   self.nextGamestate.position - vector(self.cam:pos())
            self.speed           =   len_vector:len() / (TRANSITION.TIME)
            self.direction       =   len_vector:normalized()
            self.zoomspeed       =   (self.nextGamestate.zoom - self.cam.zoom) / (TRANSITION.TIME)
            self.rotspeed        =   math.rad(self.nextGamestate.rotation - math.deg(self.cam.rot)) / (TRANSITION.TIME)
        else
             if self.ELAPSED<=self.TIME then
                self.cam:move((self.direction*self.speed*dt):unpack())
                self.cam:rotate(self.rotspeed*dt)
                self.cam.zoom = self.cam.zoom + self.zoomspeed*dt
            else
                self.cam.zoom   =   self.nextGamestate.zoom
                self.cam.rot    =   math.rad(self.nextGamestate.rotation)

                if self.nextGamestate then -- If it's a gamestate transition, else: it's a camera move
                    Gamestate.switch(self.nextGamestate)
                end
                self.ONGOING      =   false
                self.speed        =   nil
                self.direction    =   nil
                self.zoomspeed    =   nil
                self.rotspeed     =   nil
                self.nextGamestate=   nil
                self.ELAPSED      =   0
            end
        end
    end
end

function TRANSITION:moveTo(p_x, p_y)
    assert(p_x, "No X destination")
    assert(p_y, "No Y destination")
    local len_vector     =   vector(p_x, p_y) - vector( self.cam:pos() )
    self.speed           =   len_vector:len() / (TRANSITION.TIME)
    self.direction       =   len_vector:normalized()
    self.zoomspeed       =   0
    self.rotspeed        =   0

end

function track_point(p_cam, point)

    c_x, c_y    =   p_cam:pos()
    x_speed, y_speed    =   0, 0
    offset      =   75
    speed       =   2.75

    y_distance  =   math.abs(c_y - point.y)
    x_distance  =   math.abs(c_x - point.x)


    -- Speed of the cam
    if y_distance > offset then
        y_speed =   speed * math.log(y_distance-offset)
    end

    if x_distance > offset then
        x_speed =   speed * math.log(x_distance-offset)
    end

    -- Direction of the cam
    if c_x > point.x then
        x_speed =   -x_speed
    end
    if c_y > point.y then
        y_speed =   -y_speed
    end
    p_cam:move(x_speed, y_speed)

end
