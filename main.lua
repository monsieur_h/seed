require "lib.slither"
require "lib.resource_loader"

require "class.player.Species"
require "class.player.Seed"
require "class.player.Core"
require "class.player.Tree"
require "class.player.Branch"
require "class.player.GhostBranch"
require "class.player.Root"
require "class.player.GhostRoot"
require "class.player.Leaf"
require "class.player.GhostLeaf"

require "class.utils.Config"
require "class.utils.Rectangle"
require "class.utils.SpecialEffect"
require "class.utils.GameDirector"

require "class.environment.Level"
require "class.environment.WaterSource"
require "class.environment.SkyManager"
require "class.environment.Sun"
require "class.environment.CloudLayer"
require "class.environment.Cloud"
require "class.environment.SunrayLayer"
require "class.environment.Nutriment"
require "class.environment.Sunray"

require "class.gui.Button"
require "class.gui.RadialMenu"
require "class.gui.ImageButton"
require "class.gui.MenuItem"
require "class.gui.ResourceBar"
require "class.gui.Helper"
require "class.gui.Tooltip"

--Translation code:
require "data.i18n.translator"
require "data.i18n.fr"

--HUMP lib (vrld)
Gamestate = require "lib.hump.gamestate"
ringbuffer= require "lib.hump.ringbuffer"
camera    = require "lib.hump.camera"
vector    = require "lib.hump.vector"

love.filesystem.load("lib/table.save.lua")()


APP_NAME        =   "Seed"
PROJECT_VERSION =   " alpha 0.0.2c"


function love.load()
    -- Init gamestates and display
    Gamestate.registerEvents()
    print(APP_NAME..PROJECT_VERSION)
    love.graphics.setCaption(APP_NAME..PROJECT_VERSION)
    GAMECONFIG      =   Config()
    setLocale(GAMECONFIG.options["locale"])
    --Set resolution here

    --SFX configuration
    FX              =   SpecialEffect()
    if GAMECONFIG.options["post-process"]=="on" then FX.active = true end
    --~ FX:loadShader("bloom")
    --~ FX:getEffect("bloom"):send("size", {love.graphics.getWidth(), love.graphics.getHeight()})
    if FX.active then
        FX:loadShader("rays")
    end
    --~ b   =   nil
    --~ FX:stop()
    -- Record the gametime
    gametime        =   0


    -- Loading fonts
    title_font      =   love.graphics.newFont("res/fonts/alamain1.ttf", 89)
    big_font        =   love.graphics.newFont("res/fonts/alamain1.ttf", 42)
    standard_font   =   love.graphics.newFont("res/fonts/alamain1.ttf", 15)
    debug_font      =   love.graphics.newFont("res/fonts/alamain1.ttf",11)
    small_font      =   debug_font
    love.graphics.setFont(standard_font)

    -- Player helper:
    hint    =   Helper()
    hint:setFont( standard_font )
    hint:setFile( "hints" )

    -- Gamestates
    require "menu_gamestates"
    require "play_gamestates"

    --Switching to default mode: newgame
    Gamestate.switch(splashscreen)



end

function love.keyreleased(key)
    if key=="d" then
        DEBUG   =   not DEBUG
    elseif key == "p" then
        --~ print("Toggling shaders")
        --~ if GAMECONFIG.options["post-process"] == "on" then
            --~ GAMECONFIG.options["post-process"] = "off"
            --~ FX.active   =   false
        --~ else
            --~ GAMECONFIG.options["post-process"] = "on"
            --~ FX.active   =   true
        --~ end

    end

end

function love.draw()
    love.graphics.setFont(standard_font)
    love.graphics.setColor(255, 255, 255)


end

function love.update(dt)
    gametime    =   gametime + dt
    hint:update(dt)
end

function love.quit()
    if hint then
        print("Saving hint data...")
        hint:saveToFile()
    end
    print("Played "..gametime.." seconds, leaving game...")

end

-- Util function to recursively print userdata
function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    else
      print(tostring(formatting) .. tostring(v))
    end
  end
end

function watermark()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(debug_font)
    love.graphics.printf(APP_NAME..PROJECT_VERSION, 0, 5, love.graphics.getWidth(), "center" )
end

function debug_draw_stats(p_cam)
    love.graphics.setFont(debug_font)
    love.graphics.setColor(255, 255, 255, 255)
    local  msg  =   "FPS:"..love.timer.getFPS()
    msg = msg.."\nPost-processing: "..GAMECONFIG.options["post-process"]
    msg = msg.."\nMouse position:\n cam: ("..love.mouse.getX()..","..love.mouse.getY()..")"
    if p_cam then
        msg =   msg.."\n"
        local x, y = p_cam:worldCoords(love.mouse.getPosition())
        msg =   msg.."\n world: ("..math.floor(x)..","..math.floor(y)..")"
        msg =   msg.."\nCamera:\n zoom: "..math.floor(p_cam.zoom*1000)/1000
        msg =   msg.."\n rotation: "..math.floor(math.deg(p_cam.rot)*1000)/1000
        x, y = p_cam:pos()
        msg =   msg.."\n center: ".."("..math.floor(x)..","..math.floor(y)..")"
    end

    if player then
        msg =   msg.."\n"
        msg =   msg.."\nNutriment:"..player.resources.nutriment.current.."/"..player.resources.nutriment.max
        msg =   msg.."\nWater:"..player.resources.water.current.."/"..player.resources.water.max
        msg =   msg.."\nLight:"..player.resources.light.current.."/"..player.resources.light.max


    end


    love.mouse.setGrab(false)
    love.graphics.print(msg, 10, 50)
end

function debug_draw_shapes(p_group)
    for i,obj in pairs(p_group) do
        if obj.hook then
            love.graphics.setColor(100, 255, 100)
            love.graphics.circle("fill", obj.hook.x, obj.hook.y, 10)
        end
        if obj.anchor then
            love.graphics.setColor(255, 100, 100)
            love.graphics.circle("fill", obj.anchor.x, obj.anchor.y, 10)
        end
    end
    love.graphics.setColor(255, 255, 255)
end

function clamp(input, min_val, max_val)
    if input < min_val then
        input = min_val
    elseif input > max_val then
        input = max_val
    end
    return input
end

function distance(p1, p2)
    assert(p1.x, "P1 has no X coordinate")
    assert(p1.y, "P1 has no Y coordinate")
    assert(p2.x, "P1 has no X coordinate")
    assert(p2.y, "P1 has no Y coordinate")
    local x =   p1.x - p2.x
    x       =   x * x
    local y =   p1.y - p2.y
    y       =   y * y
    return math.sqrt(x + y)

end


-- TODO refactor this
-- accepts point and angle in radians
function change_angle ( x, y, a )
  local cd = math.cos ( a )
  local sd = math.sin ( a )
  return cd*x - sd*y, cd*y + sd*x
end

-- accepts a point (x, y), aabb center (rx, ry), aabb half-width/half-height (hw, hh) and an angle in radians (a)
function point_vs_raabb ( x, y, rx, ry, hw, hh, a )
  -- translate point to aabb origin
  local lx, ly = x - rx, y - ry
  -- rotate point
  lx, ly = change_angle ( lx, ly, a )
  -- perform regular point vs aabb test
  if lx < -hw or lx > hw then
     return false
  end
  if ly < -hh or ly > hh then
     return false
  end
  return true
end

-- util for converting image red channel to alpha channel
function rgbtoa_map( x, y, r, g, b, a )
    --return 255,255,255,255-r
    return 255,255,255,r
end
