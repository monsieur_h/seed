------------------------------------------------------------------------
-- @file translator.lua
-- defines function for language translation
-- supports en, fr
------------------------------------------------------------------------



text    = {}
locale  = "en"
function setLocale(p_locale)
    locale      =   p_locale or "en"

end

-- alias of gettext() function: returns a string in chosen locale
function _(p_string)
    if text[locale] then
        return text[locale][p_string] or p_string
    else
        return p_string
    end
end

