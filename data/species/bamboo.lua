    return{
    -- User shown data
    name                =   "Bamboo",
    description         =   "The bamboo grows straight and high. With very little need for nutriments.",
    preview_image       =   "bamboo/preview.png",
    preview_icon        =   "bamboo/icon.png",
    max_jumps           =   2,
    max_nutriment       =   120,
    max_water           =   80,
    max_light           =   100,
    start_nutriment     =   50,
    start_water         =   50,
    start_light         =   50,

    -- Seed
    seed_image          =   "bamboo/seed.png",
    seed_description	=	"This is your seed, the base of your bamboo.",
    seed_shape          =   {50,0, 38,46, 50,97, 61,50},
    seed_weight         =   0.5,

    --Branch
    branch_image        =   {[1]="bamboo/branch_top.png", [2]="bamboo/branch.png"},
    branch_description	=	"This is a bamboo branch. It allows you to grow leaves and to go higher.",
    branch_image_empty  =   "bamboo/branch_empty.png",
    branch_placeholder  =   "bamboo/branch_placeholder.png",
    branch_angle        =   10,
    max_branches        =   1,
    branch_hook         =   {[1]={x=250,y=280},[2]={x=50, y=10} },
    branch_anchor       =   {[1]={x=250,y=737},[2]={x=50, y=465}},	--One pairs of x,y coordinates for each level of branch
    branch_level_factor =   15,
    branch_range        =   60,    -- unused yet
    branch_cost         =   {light=15, water=10, nutriment=20},
    branch_uptake       =   {light=0.5, water=0.5, nutriment=0.5},
    branch_collect      =   {light=1, water=0, nutriment=0}, --On first level, the branch can collect light (it has some leaves on it)

    -- Root
    root_image          =   "bamboo/root.png",
    root_placeholder    =   "bamboo/root_placeholder.png",
    root_description	=	"This a bamboo root, it allows you to collect nutriments and water",
    root_angle          =   110,
    root_hook           =   {54, 289},
    max_roots           =   3,
    root_anchor         =   {50, 10},
    root_level_factor   =   15,
    root_cost           =   {light=25, water=15, nutriment=10},
    root_range          =   60,    -- range to get nutriments
    root_uptake         =   {light=0.5, water=0, nutriment=0},

    --Leaf
    leaf_image          =   "bamboo/leaf.png",
    leaf_placeholder    =   "bamboo/leaf_placeholder.png",
    leaf_description	=	"A typical bamboo leaf, collecting light during the day",
    leaf_angle          =   120,
    leaf_anchor         =   {167, 505},
    leaf_hook           =   {156, 335},
    max_leafs           =   1,
    leaf_max_recursion  =   2,
    leaf_cost           =   {light=0, water=10, nutriment=10},
    leaf_level_factor   =   100,
    leaf_uptake         =   {light=0, water=0.5, nutriment=0.5}
}
