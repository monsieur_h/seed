------------------------------------------------------------------------
-- Helper data to send as infos
-- Current how many times we send it, max is the maximum time we say it
-- 0 means infinte time
-- This file is checked with `hint:safeSend("Hello World")`
-- Random hint will be printed in order
------------------------------------------------------------------------

local a = {
    ["Right-click to move to the previous menu"]= {current=0, max=5},
    ["The play button is off duty press enter to confirm."]= {current=0, max=5},
    ["Hello World"] = {current=0, max=2},
    ["Zoom with mouse wheel"] = {current=0, max=1, random=true},
    ["Hold left click to move around"] = {current=0, max=1, random=true},
    ["Beginning with roots is often a good idea"] = {current=0, max=1, random=true},
    ["Every part you grow will uptake resources to live"] = {current=0, max=1, random=true},
    ["Branches are the only elment that harvest nothing, don't grow too many"] = {current=0, max=1, random=true},
    ["Theses hints will only appear once"] = {current=0, max=1, random=true},
    ["This is a test version, please report any bug"] = {current=0, max=1, random=true},
    ["version"]     =   PROJECT_VERSION or "unknown"
}
return a
