#Seed

Welcome to Seed, a game project where you live the life of a plant! The project is currently under heavy development and is in alpha version. If you don't get it, it means that most version aren't stable enough to be launched or played.

To try it out and see how it feels you can clone this repository and use the **.bat** files. You will need to have [L�VE framework](https://love2d.org/ installed. )

If you are running linux you can use the following command:

    love /path/to/the/git/folder/of/seed

And you will need L�VE (package called *love* in most distros' repositories).

The final version will be standalone and won't require any installation.
