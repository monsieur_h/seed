
---My class-------------------------------------------------------------
class "Core" {
    speed   =   0,
    x_pos   =   0,
    y_pos   =   0,
    x_direction =   1,
    y_direction =   1
}

function Core:__init__(p_image)
    self.image  =   love.graphics.newImage("res/images/sprites/core.png")
    --Positionning at x:center,y:80%
    self.y_pos  =   love.graphics.getHeight() / 5 *4
    self.y_pos  =   self.y_pos - self.image:getHeight() / 2
    self.x_pos  =   love.graphics.getWidth() / 2
    self.x_pos  =   self.x_pos - self.image:getWidth() / 2
end

function Core:update(p_dt)
    --~ TODO:Updates to the core
    --~ self.x_pos   =   self.x_pos   +   (self.speed   *   p_dt *   self.x_direction)
    --~ self.y_pos   =   self.y_pos   +   (self.speed   *   p_dt *   self.y_direction)
    --X collision
    --~ if  self.x_pos + self.image:getWidth() >= love.graphics.getWidth() then
        --~ self.x_direction =   self.x_direction *   -1
    --~ end
    --~ if self.x_pos < 0 then
        --~ self.x_direction =   self.x_direction *   -1
    --~ end
--~
    --~ --Y collision
    --~ if self.y_pos < 0 then
        --~ self.y_direction =   self.y_direction *   -1
    --~ end
    --~ if  self.y_pos + self.image:getHeight() >= love.graphics.getHeight() then
        --~ self.y_direction =   self.y_direction *   -1
    --~ end

end

function Core:getDrawable()
    return self.image, self.x_pos, self.y_pos
end

---/My class------------------------------------------------------------
