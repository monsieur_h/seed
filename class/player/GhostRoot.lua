------------------------------------------------------------------------
-- @class GhostRoot
-- Class representing a dummy for placing a root
-- GhostRoot of the player
------------------------------------------------------------------------

class "GhostRoot"{
}

function GhostRoot:__init__(p_parent, p_hook)
    print("new ghostroot")
    self.parent     =   p_parent
    self.species    =   p_parent.species
    self.image      =   self.species.root_placeholder
    self.anchor     =   {}
    self.hook       =   {}
    self.center     =   {}
    self.angle      =   0
    self.time       =   0
    self.dead       =   false
    self.v_hook     =   {x=0,y=0} -- Virtual hook
    self.anchor.x, self.anchor.y    =   unpack(self.species.root_anchor)
    self.hook.x, self.hook.y        =   unpack(self.species.root_hook)
    self.x_pos, self.y_pos          =   0,0
    if p_hook then
        self.x_pos, self.y_pos  =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos  =   p_parent.hook.x, p_parent.hook.y
    end
    print("Ghost root created with pos: "..self.x_pos.." "..self.y_pos)
end

function GhostRoot:testPoint(p_x, p_y)
    return false

end

function GhostRoot:onClick(p_x, p_y)
    return false

end


function GhostRoot:update(dt)
    self.time   =   self.time + dt
    local x,y   =   seedcam:worldCoords(love.mouse.getPosition()) --TODO: AAAAAAAAAAAH fix that, fix THAT NOW!
    self.angle  =   math.atan2(x-self.x_pos, y-self.y_pos)
    self.angle  =   -self.angle



    -- TODO refactor this, once upon a time (low priority, it works)
    local max_a = math.rad(self.species.root_angle/2)
    local min_a = math.rad(-self.species.root_angle/2)

    if self.angle > max_a then
        self.angle  =   max_a
    elseif self.angle < min_a then
        self.angle  =   min_a
    end



    --Calculating hook position
    self.v_hook  =   vector(self.hook.x - self.anchor.x, self.hook.y - self.anchor.y)
    self.v_hook:rotate_inplace(self.angle)
    self.center.x, self.center.y    =   self.v_hook.x/2+self.x_pos, self.v_hook.y/2+self.y_pos
    self.v_hook.x, self.v_hook.y    =   self.v_hook.x + self.x_pos, self.v_hook.y+self.y_pos

    --~ self.hook.x, self.hook.y        =   v_hook.x+self.x_pos, v_hook.y+self.y_pos

    if self.nutriments then
        self:hoverNutriment()
    end

end

function GhostRoot:hoverNutriment()
    for i, nut in pairs(self.nutriments) do
        if distance(self.v_hook, nut.pos)<self.species.branch_range or distance(self.center, nut.pos)<self.species.branch_range then
            nut.hovered =   true
        else
            nut.hovered =   false
        end

    end

end

function GhostRoot:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self:getDrawable())

    if self.nutriments then
        for i, nut in ipairs(self.nutriments) do
            if nut.hovered then
                if distance(self.v_hook, nut.pos) < distance(self.center, nut.pos) then
                    origin  =   self.v_hook
                else
                    origin  =   self.center
                end
                love.graphics.setLine(5, "smooth")
                love.graphics.setColor(255, 255, 255, 150)
                love.graphics.line(origin.x, origin.y, nut.pos.x, nut.pos.y)
            end
        end
    end
    if DEBUG then
        love.graphics.setColor(255,0,0)
        love.graphics.circle("fill",self.anchor.x, self.anchor.y, 10)
        love.graphics.setColor(0,255,0)
        love.graphics.circle("fill",self.x_pos, self.y_pos, 10)
        love.graphics.setColor(0,0,255)
        love.graphics.circle("fill", self.v_hook.x, self.v_hook.y, 10)
    end

end

function GhostRoot:getDrawable()
    return self.image, self.x_pos, self.y_pos, self.angle, 1, 1, self.anchor.x, self.anchor.y
--~ --~
end

function GhostRoot:getTree()
    return self.parent:getTree()
end

function GhostRoot:confirm()
    print("Root confirmed")
    self.parent:confirmRoot(self)
    self.dead   =   true
end

function GhostRoot:cancel()
    print("Root cancelled")
    local t = self:getTree()
    t.appending_element =   nil
    t.appending         =   false
    self.dead           =   true

end
