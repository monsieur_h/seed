------------------------------------------------------------------------
-- @class Tree
-- Class representing a player's treen
-- Contains branches and roots. Can append/decay them and make fruits
------------------------------------------------------------------------

class "Tree"{
}

function Tree:__init__(p_species)
    self.core           =   {}
    self.branches       =   {}
    self.roots          =   {}
    self.leafs          =   {}
    self.species        =   p_species
    self.branch_image   =   p_species.branch_image
    self.root_image     =   p_species.root_image
    self.transparency   =   true
    self.appending      =   false
    self.resources      =   {}
    self.time			=	0
    self.resources.nutriment    =   {current=p_species.start_nutriment or 0.0, max=p_species.max_nutriment or 100.0, preview=0.0, lastCantAffordTime=0}
    self.resources.water        =   {current=p_species.max_water or 0.0, max=p_species.max_water or 100.0, preview=0.0, lastCantAffordTime=0}
    self.resources.light        =   {current=p_species.max_light or 0.0, max=p_species.max_light or 100.0, preview=0.0, lastCantAffordTime=0}
    self.lastCantAffordTime		=	0
    
    print("New player tree: "..self.species.name)
end

function Tree:draw()
    self:drawSurfaceParts()
    self:drawUnderGroundParts()


end

function Tree:drawUnderGroundParts()
    --Draw my roots
    for i, r in pairs(self.roots) do
        r:draw()
    end


    --Draw my seed
    if self.core.draw then
        self.core:draw()
    end

end

function Tree:drawSurfaceParts()
    --Draw my leaves
    for i, l in pairs(self.leafs) do
        l:draw()
    end

    --Draw my branches
    for i, b in pairs(self.branches) do
        b:draw()
    end
end

function Tree:cancel()
    self.appending_element:cancel()
    self.resources.nutriment.preview    =   0
    self.resources.water.preview        =   0
    self.resources.light.preview        =   0

end

function Tree:newBranch(p_parent)
    new_branch  =   Branch(p_parent)
    table.insert(self.branches, new_branch)
    print("Branch: "..new_branch.x_pos.." "..new_branch.y_pos)
    return new_branch

end

function Tree:newRoot(p_root)
    new_root    =   Root(p_root)
    table.insert(self.roots, new_root)
    print("New root added to tree")
    return new_root

end

function Tree:setTransparent(p_bool)
    print("Setting transparency on tree")
    print(p_bool)
    self.transparent    =   p_bool
    for idx,branch in pairs(self.branches) do
        branch:setTransparent(p_bool)
    end

end

function Tree:setWorld(p_world)
    self.world  =   p_world

end

function Tree:setCamera(p_cam)
    self.camera =   p_cam

end

function Tree:getTree()
    return self
end

function Tree:setCore(seed)
    seed.parent =   self
    self.core   =   seed
end

function Tree:testPoint(p_x, p_y)
    -- Testing seed
    if seed:testPoint(p_x, p_y) then
        return seed
    end

    --Testing leaves
    for id, leaf in pairs(self.leafs) do
        if leaf:testPoint(p_x, p_y) then
            return leaf
        end
    end

    --Testing branches
    for id, branch in pairs(self.branches) do
        if branch:testPoint(p_x, p_y) then
            return branch
        end
    end

    --Testing roots
    for id, root in pairs(self.roots) do
        if root:testPoint(p_x, p_y) then
            return root
        end
    end
end

function Tree:update(dt)
	self.time	=	self.time + dt
    --Update my branches
    for i, b in pairs(self.branches) do
        b:update(dt)
    end

    --Update my roots
    for i, r in pairs(self.roots) do
        r:update(dt)
    end

    --Update my leaves
    for i, l in pairs(self.leafs) do
        l:update(dt)
    end
    --Update my seed
    if self.core.update then
        self.core:update(dt)
    end
    --Updating resources

    local r =   self.resources
    r.water.current     =   clamp(r.water.current, 0, r.water.max)
    r.nutriment.current =   clamp(r.nutriment.current, 0, r.nutriment.max)
    r.light.current     =   clamp(r.light.current, 0, r.light.max)
end

function Tree:connectNutriment(p_nutriment)
    for i, root in pairs(self.roots) do
        root:connect(p_nutriment)
    end

end

function Tree:buyRoot()
    local r   =   self.resources
    if (r.light > self.species.root_cost.light
    and r.water > self.species.root_cost.water
    and r.nutriment > self.species.root_cost.nutriment ) then
        r.light    =    r.light - self.species.root_cost.light
        r.water    =    r.light - self.species.root_cost.water
        r.nutriment=    r.light - self.species.root_cost.nutriment
        return true

    else
        return false
    end

end

function Tree:canAffordBranch()
    local r   =   self.resources
    local afford = true
    if r.light.current < self.species.branch_cost.light then
		r.light.lastCantAffordTime = self.time
		afford = false
	elseif r.water.current < self.species.branch_cost.water then
		r.water.lastCantAffordTime = self.time
		afford = false
	elseif r.nutriment.current < self.species.branch_cost.nutriment then
		r.nutriment.lastCantAffordTime = self.time
		afford = false
	end
	return afford
end

function Tree:canAffordRoot()
	local r   =   self.resources
	local afford = true
    if r.light.current < self.species.root_cost.light then
		r.light.lastCantAffordTime = self.time
		afford = false
		
	elseif r.water.current < self.species.root_cost.water then
		r.water.lastCantAffordTime = self.time
		afford = false
		
	elseif r.nutriment.current < self.species.root_cost.nutriment then
		r.nutriment.lastCantAffordTime = self.time
		afford = false
	end
	return afford
end

function Tree:canAffordLeaf()
	local r   =   self.resources
	local afford = true
    if r.light.current < self.species.leaf_cost.light then
		r.light.lastCantAffordTime = self.time
		afford = false
	elseif r.water.current < self.species.leaf_cost.water then
		r.water.lastCantAffordTime = self.time
		afford = false
	elseif r.nutriment.current < self.species.leaf_cost.nutriment then
		r.nutriment.lastCantAffordTime = self.time
		afford = false
	end
	return afford
end

function Tree:buyBranch()
    local r   =   self.resources
    if self:canAffordBranch() then
        r.light.current    =    r.light.current - self.species.branch_cost.light
        r.water.current    =    r.water.current - self.species.branch_cost.water
        r.nutriment.current=    r.nutriment.current - self.species.branch_cost.nutriment
        return true
    else
        return false
    end
end

function Tree:buyRoot()
    local r   =   self.resources
    if self:canAffordRoot() then
        r.light.current    =    r.light.current - self.species.root_cost.light
        r.water.current    =    r.water.current - self.species.root_cost.water
        r.nutriment.current=    r.nutriment.current - self.species.root_cost.nutriment
        return true

    else
        return false
    end
end

function Tree:buyLeaf()
    local r   =   self.resources
    if self:canAffordLeaf() then
        r.light.current    =    r.light.current - self.species.leaf_cost.light
        r.water.current    =    r.water.current - self.species.leaf_cost.water
        r.nutriment.current=    r.nutriment.current - self.species.leaf_cost.nutriment
        return true

    else
        return false
    end
end
