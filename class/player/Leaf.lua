------------------------------------------------------------------------
-- @class Branch
-- Class representing a player's branch
-- Leaf of the player, can grow or or decay or do whatever branches do
------------------------------------------------------------------------

class "Leaf"{
}

function Leaf:__init__(p_parent, p_hook)
    print("new root")
    self.parent     =   p_parent
    self.depth      =   p_parent.depth or 0
    self.depth      =   self.depth + 1
    self.species    =   p_parent.species
    self.image      =   self.species.leaf_image
    self.anchor     =   {}
    self.hook       =   {}
    self.center     =   {}
    self.leafs      =   {}
    self.level      =   0
    self.angle      =   0
    self.time       =   0
    self.selected   =   false
    self.hook.x, self.hook.y        =   unpack(self.species.leaf_hook)
    self.anchor.x, self.anchor.y    =   unpack(self.species.leaf_anchor)
    self.x_pos, self.y_pos          =   0,0
    if p_hook then
        self.x_pos, self.y_pos      =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos      =   p_parent.hook.x, p_parent.hook.y
    end
end

function Leaf:setTransparent(p_bool)
    self.transparent    =   p_bool

end

function Leaf:update(dt)
    self.time   =   self.time + dt

    -- Resources uptake
    local nutriment =   -self:uptakeNutriment(dt)
    local water     =   -self:uptakeWater(dt)
    local light     =   self:collectLight(dt) - self:uptakeLight(dt)
    self:getTree().resources.nutriment.current  =   self:getTree().resources.nutriment.current  + nutriment
    self:getTree().resources.water.current      =   self:getTree().resources.water.current      + water
    self:getTree().resources.light.current      =   self:getTree().resources.light.current      + light

end

function Leaf:uptakeNutriment(dt)
    return self.species.leaf_uptake.nutriment * dt
end

function Leaf:uptakeWater(dt)
    return self.species.leaf_uptake.water * dt
end

function Leaf:uptakeLight(dt)
    return self.species.leaf_uptake.light * dt
end

function Leaf:collectLight(dt)
    return 1 * dt --TODO: rays casting check with sun, day/night cycle
end

function Leaf:levelUp()
    self.level  =   self.level + 1
    self.parent:levelUp()
end

function Leaf:confirmLeaf(p_ghost)
    local t = self:getTree()
    if t:buyLeaf() then
        local b = Leaf(self)
        b:fromGhost(p_ghost)
        table.insert(self.leafs, b)
        table.insert(self:getTree().leafs, 0, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        return true
    else
        t:cancel()
        return false
    end
end

function Leaf:draw()
    if self.selected then
        love.graphics.setColor(255, 100, 100)
    else
        love.graphics.setColor(200, 200, 200)
    end
    love.graphics.draw(self:getDrawable())

    if DEBUG then
        love.graphics.setColor(255,0,0)
        love.graphics.circle("fill",self.anchor.x, self.anchor.y, 10)
        love.graphics.setColor(0,255,0)
        love.graphics.circle("fill",self.x_pos, self.y_pos, 10)
        love.graphics.setColor(0,0,255)
        love.graphics.circle("fill", self.hook.x, self.hook.y, 10)
        love.graphics.setColor(255,255,255)
        love.graphics.circle("fill", self.center.x, self.center.y, 15)
    end
end

function Leaf:fromGhost(p_ghost)
    self.side   =   p_ghost.side
    if self.side == -1 then
        self.anchor.x =     self.image:getWidth() - p_ghost.anchor.x
        self.anchor.y =     p_ghost.anchor.y
        self.hook.x   =   self.image:getWidth() - self.hook.x
    else
        self.anchor =   p_ghost.anchor
    end
        self.x_pos  =   p_ghost.x_pos
        self.y_pos  =   p_ghost.y_pos
        self.angle  =   p_ghost.angle
    local v_hook  =   vector(self.hook.x - self.anchor.x, self.hook.y - self.anchor.y)
    v_hook:rotate_inplace(self.angle)
    self.hook.x, self.hook.y        =   v_hook.x+self.x_pos, v_hook.y+self.y_pos
    self.center.x, self.center.y    =   v_hook.x/2+self.x_pos, v_hook.y/2+self.y_pos

end

function Leaf:getDrawable(p_image)
    local scale =   1 + (self.level/self.species.leaf_level_factor)
    if not p_image then

        return self.image, self.x_pos, self.y_pos, self.angle, scale * self.side, 1, self.anchor.x, self.anchor.y
    else
        return p_image, self.x_pos, self.y_pos, self.angle, scale * self.side, 1, self.anchor.x, self.anchor.y
    end

end

function Leaf:getTree()
    return self.parent:getTree()
end

function Leaf:appendLeaf()

    local t = self:getTree()
    if t:canAffordLeaf() then
        local b = GhostLeaf(self)
        t.appending_element =   b
        t.appending         =   true
        t.resources.nutriment.preview   =   t.species.leaf_cost.nutriment
        t.resources.water.preview       =   t.species.leaf_cost.water
        t.resources.light.preview       =   t.species.leaf_cost.light

        --Correct this
        table.insert(GAME_OBJECTS, b)
        b=nil
        return true
    else
        print("Can't afford a leaf yet")
        self.selected   =   false
        return false
    end
end

function Leaf:getActions()
    local a = {}
    if #self.leafs < self.species.max_leafs and self.depth < self.species.leaf_max_recursion then
        table.insert(a, "Leaf")
    end
    table.insert(a, "Decay")
    table.insert(a, "Inspect")
    return a
end

function Leaf:testPoint(p_x, p_y)
    local test = point_vs_raabb (p_x, p_y, self.center.x, self.center.y , self.image:getWidth()/2, self.image:getHeight()/2, math.pi*2 - self.angle)
    return test


end

function Leaf:getToolTipInfo()
	--~ local text = self.species.leaf_description .. "\n"
	local text = ""
	text = text .. _("Collecting") .. "\n"
	text = text .. _("light") .. " : " .. 1 .. "\n"
	text = text .. _("Uptaking") .. "\n"
	text = text .. _("nutriments") .. " : " .. self.species.leaf_uptake.nutriment .. "\n"
	text = text .. _("water") .. " : " .. self.species.leaf_uptake.water .. "\n"
	text = text .. _("light") .. " : " .. self.species.leaf_uptake.light .. "\n"
	return text
end

function Leaf:inspect()
	local tt = Tooltip( self, self.center, self:getToolTipInfo() )
	return tt
end
