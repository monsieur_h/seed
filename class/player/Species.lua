------------------------------------------------------------------------
-- @class Species
-- Class representing a plant species. It contains a seed image, branch
-- images. Also gives max number of segment and their angle.
------------------------------------------------------------------------

class "Species"{
    attributes  =   "goes there"
}

function Species:__init__(p_attributes)
    -- TODO: read infos from file
    self.name           =   p_attributes.name
    self.description    =   p_attributes.description
    self.branch_image   =   p_attributes.branch_image
    self.seed_image     =   p_attributes.seed_image
    self.root_image     =   p_attributes.root_image
    self.max_branches   =   p_attributes.max_branches
    self.max_roots      =   p_attributes.max_roots
    self.max_jumps      =   p_attributes.max_jumps
    self.root_angle     =   p_attributes.root_angle
    self.branch_angle   =   p_attributes.branch_angle
    self.branch_image_empty =   p_attributes.branch_image_empty

end

--~ function Species:getSeed(p_world)
    --~ print("seed getting")
    --~ if p_world then
        --~ s   =   Seed(10, 10)
    --~ else
        --~ s   =   Seed(10, 10, p_world)
    --~ end
--~
    --~ s:setImage(self.seed_image)
    --~ s:setJumpLeft(self.max_jumps)
    --~ s:setPos(10,10)
    --~ return s
--~ end
