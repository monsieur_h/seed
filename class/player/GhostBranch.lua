------------------------------------------------------------------------
-- @class GhostGhostBranch
-- Class representing a player's branch
-- GhostBranch of the player, can grow or or decay or do whatever GhostBranches do
------------------------------------------------------------------------

class "GhostBranch"{
}

function GhostBranch:__init__(p_parent, p_hook)
    print("new branch")
    self.parent     =   p_parent
    self.species    =   p_parent.species
    self.image      =   self.species.branch_placeholder
    self.anchor     =   {}
    self.angle      =   0
    self.time       =   0
    self.dead       =   false
    self.anchor.x, self.anchor.y    =   self.species.branch_anchor[1].x, self.species.branch_anchor[1].y
    self.x_pos, self.y_pos          =   0,0
    if p_hook then
        self.x_pos, self.y_pos  =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos  =   p_parent.hook.x, p_parent.hook.y
    end
    print("Ghost created with pos: "..self.x_pos.." "..self.y_pos)
end

function GhostBranch:testPoint(p_x, p_y)
    return false

end

function GhostBranch:onClick(p_x, p_y)
    return false

end


function GhostBranch:update(dt)
    --~ print(self.x_pos)
    --~ print(self.y_pos)
    self.time   =   self.time + dt
    local x,y   =   seedcam:worldCoords(love.mouse.getPosition()) --TODO: AAAAAAAAAAAH fix that, fix THAT NOW!
    self.angle  =   math.atan2(x-self.x_pos, y-self.y_pos)
    self.angle  =   -self.angle + math.pi


    -- TODO refactor this, once upon a time (low priority, it works)
    local max_a = math.rad(self.species.branch_angle/2)
    local min_a = math.rad(360-self.species.branch_angle/2)

    if self.angle > max_a and self.angle < math.pi then
        self.angle  =   max_a
    elseif self.angle < min_a and self.angle > math.pi then
        self.angle  =   min_a
    end

end

function GhostBranch:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self:getDrawable())

end

function GhostBranch:getDrawable()
    return self.image, self.x_pos, self.y_pos, self.angle, 1, 1, self.anchor.x, self.anchor.y
--~ --~
end

function GhostBranch:getTree()
    return self.parent:getTree()
end

function GhostBranch:confirm()
    print("Branch confirmed")
    self.parent:confirmBranch(self)
    self.dead   =   true

end

function GhostBranch:cancel()
    print("Branch cancelled")
    local t = self:getTree()
    t.appending_element =   nil
    t.appending         =   false
    self.dead   =   true

end
