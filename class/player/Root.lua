------------------------------------------------------------------------
-- @class Branch
-- Class representing a player's branch
-- Root of the player, can grow or or decay or do whatever branches do
------------------------------------------------------------------------

class "Root"{
}

function Root:__init__(p_parent, p_hook)
    print("new root")
    self.parent     =   p_parent
    self.species    =   p_parent.species
    self.image      =   self.species.root_image
    self.anchor     =   {}
    self.hook       =   {}
    self.center     =   {}
    self.roots      =   {}
    self.level      =   0
    self.angle      =   0
    self.time       =   0
    self.actions    =   {"Root", "Inspect"}
    self.selected   =   false
    self.hook.x, self.hook.y        =   unpack(self.species.root_hook)
    self.anchor.x, self.anchor.y    =   unpack(self.species.root_anchor)
    self.x_pos, self.y_pos          =   0,0
    if p_hook then
        self.x_pos, self.y_pos      =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos      =   p_parent.hook.x, p_parent.hook.y
    end
end

function Root:setTransparent(p_bool)
    self.transparent    =   p_bool

end

function Root:update(dt)
    self.time   =   self.time + dt
    local nutriment =   self:collectNutriment(dt)   - self:uptakeNutriment(dt)
    local water     =   self:collectWater(dt)       - self:uptakeWater(dt)
    local light     =   -self:uptakeLight(dt)
    self:getTree().resources.nutriment.current  =   self:getTree().resources.nutriment.current  + nutriment
    self:getTree().resources.water.current      =   self:getTree().resources.water.current      + water
    self:getTree().resources.light.current      =   self:getTree().resources.light.current      + light
end

function Root:uptakeNutriment(dt)
    return self.species.root_uptake.nutriment * dt
end

function Root:uptakeWater(dt)
    return self.species.root_uptake.water * dt
end

function Root:uptakeLight(dt)
    return self.species.root_uptake.light * dt
end

function Root:collectNutriment(dt)
    if self.nutriments then
        local sum = 0
        for i, nut in pairs(self.nutriments) do
            sum = sum + nut:collect( dt )
        end
        --~ self:getTree().resources.nutriment.current  =   self:getTree().resources.nutriment.current + #self.nutriments * dt
        --~ self:getTree().resources.water.current      =   self:getTree().resources.water.current     + 1 * dt
        return sum
    end
end

function Root:collectWater(dt)
    return 1 * dt --TODO: water spot connection
end

function Root:levelUp()
    self.level  =   self.level + 1
    self.parent:levelUp()
end

function Root:confirmRoot(p_ghost)
    local b = Root(self)
    local t = self:getTree()
    if t:buyRoot() then
        b:fromGhost(p_ghost)
        table.insert(self.roots, b)
        table.insert(self:getTree().roots, 0, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        -- If max reached: remove option
        if #self.roots == self.species.max_roots then
            for i, v in pairs(self.actions) do
                if v == "Root" then table.remove(self.actions, i) end
            end
        end
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        return true
    else
        t:cancel()
        return false
    end
end

function Root:draw()
    if self.selected then
        love.graphics.setColor(200, 200, 200)
    else
        love.graphics.setColor(255, 255, 255)
    end
    love.graphics.draw(self:getDrawable())

    --Drawing mini roots connecting to nutriments
    if self.nutriments then
        for i, nut in ipairs(self.nutriments) do
            if distance(self.hook, nut.pos) < distance(self.center, nut.pos) then
                origin  =   self.hook
            else
                origin  =   self.center
            end

            if DEBUG then
                love.graphics.setLine(8, "smooth")
                love.graphics.setColor(155, 155, 155, 150)
                love.graphics.line(origin.x, origin.y, nut.pos.x, nut.pos.y)
            else
                local angle =   -math.atan2(origin.x - nut.pos.x, origin.y - nut.pos.y) + math.pi
                local length=   distance(nut.pos, origin)
                local scale =   length/self.image:getHeight()
                love.graphics.setColor(255, 255, 255, 255)
                love.graphics.draw(self.image, origin.x, origin.y, angle, scale * 2, scale, self.image:getWidth()/2 )
            end
        end
    end


    if DEBUG then
        love.graphics.setColor(255,0,0)
        love.graphics.circle("fill",self.anchor.x, self.anchor.y, 10)
        love.graphics.setColor(0,255,0)
        love.graphics.circle("fill",self.x_pos, self.y_pos, 10)
        love.graphics.setColor(0,0,255)
        love.graphics.circle("fill", self.hook.x, self.hook.y, 10)
        love.graphics.setColor(255,255,255)
        love.graphics.circle("fill", self.center.x, self.center.y, 15)
    end
end

function Root:fromGhost(p_ghost)
    self.anchor =   p_ghost.anchor
    self.x_pos  =   p_ghost.x_pos
    self.y_pos  =   p_ghost.y_pos
    self.angle  =   p_ghost.angle
    local v_hook  =   vector(self.hook.x - self.anchor.x, self.hook.y - self.anchor.y)
    v_hook:rotate_inplace(self.angle)
    self.hook.x, self.hook.y        =   v_hook.x+self.x_pos, v_hook.y+self.y_pos
    self.center.x, self.center.y    =   v_hook.x/2+self.x_pos, v_hook.y/2+self.y_pos

end

function Root:getDrawable(p_image)
    local scale =   1 + (self.level/self.species.root_level_factor)
    if not p_image then

        return self.image, self.x_pos, self.y_pos, self.angle, scale, 1, self.anchor.x, self.anchor.y
    else
        return p_image, self.x_pos, self.y_pos, self.angle, scale, 1, self.anchor.x, self.anchor.y
    end

end

function Root:getTree()
    return self.parent:getTree()
end

function Root:appendRoot()
    local t = self:getTree()
    if t:canAffordRoot() then
        local b = GhostRoot(self)

        t.appending_element =   b
        t.appending         =   true


        t.resources.nutriment.preview   =   t.species.root_cost.nutriment
        t.resources.water.preview       =   t.species.root_cost.water
        t.resources.light.preview       =   t.species.root_cost.light

        --Correct this
        print("Root is inserting the dummy root in GUI")
        table.insert(GAME_OBJECTS, b)
        print("Gui has now: "..#GUI)
        b=nil
        return true
    else
        self.selected   =   false
        return false
    end

end

function Root:getActions()
    return self.actions
    --~ if #self.roots >= self.species.max_roots then
        --~ return {}
    --~ else
        --~ return {"Root"}
    --~ end
end

function Root:testPoint(p_x, p_y)
    local test = point_vs_raabb (p_x, p_y, self.center.x, self.center.y , self.image:getWidth()/2, self.image:getHeight()/2, math.pi*2 - self.angle)
    return test


end

function Root:distance(p_x, p_y)
    local point     =   {x=p_x, y=p_y}
    local center_d  =   distance(self.center, point)
    local hook_d    =   distance(self.hook, point)
    return math.min(center_d, hook_d)

end

function Root:connect(p_nutriments)
    if not self.nutriments then
        self.nutriments =   {}
        for i, nut in pairs(p_nutriments) do
            if distance(self.hook, nut.pos)<self.species.root_range or distance(self.center, nut.pos)<self.species.root_range then

                if not nut.selected then --If that resource is not taken by an other root...
                    nut:connect()
                    table.insert(self.nutriments, nut)
                end

            end

        end

    end

end

function Root:getToolTipInfo()
    --~ local text = self.species.leaf_description .. "\n"
    local text = ""
    text = text .. _("Collecting") .. "\n"
    if self.level == 0 then
        text = text .. _("light") .. " : " .. 1 .. "\n"
    else
        text = text .. _("light") .. " : 0 \n"
    end
    text = text .. _("Uptaking") .. "\n"
    text = text .. _("nutriments") .. " : " .. self.species.root_uptake.nutriment .. "\n"
    text = text .. _("water") .. " : " .. 1 .. "\n"
    text = text .. _("light") .. " : " .. #self.roots .. "\n"
    return text
end

function Root:inspect()
    local tt = Tooltip( self, self.center, self:getToolTipInfo() )
    return tt
end
