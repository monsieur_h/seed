------------------------------------------------------------------------
-- @class GhostLeaf
-- Class representing a dummy for placing a root
-- GhostLeaf of the player
------------------------------------------------------------------------

class "GhostLeaf"{
}

function GhostLeaf:__init__(p_parent, p_hook)
    print("new ghostleaf")
    self.parent     =   p_parent
    self.species    =   p_parent.species
    self.image      =   self.species.leaf_placeholder
    self.anchor     =   {}
    self.side       =   1
    self.angle      =   0
    self.time       =   0
    self.dead       =   false
    self.anchor.x, self.anchor.y    =   unpack(self.species.leaf_anchor)
    self.x_pos, self.y_pos          =   0,0
    if p_hook then
        self.x_pos, self.y_pos  =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos  =   p_parent.hook.x, p_parent.hook.y
    end
    print("Ghost root created with pos: "..self.x_pos.." "..self.y_pos)
end

function GhostLeaf:testPoint(p_x, p_y)
    return false

end

function GhostLeaf:onClick(p_x, p_y)
    return false

end

function GhostLeaf:update(dt)
    self.time   =   self.time + dt
    local x,y   =   seedcam:worldCoords(love.mouse.getPosition()) --TODO: AAAAAAAAAAAH fix that, fix THAT NOW!
    self.angle  =   math.atan2(x-self.x_pos, y-self.y_pos)
    self.angle  =   -self.angle + math.pi


    -- TODO refactor this, once upon a time (low priority, it works)
    local max_a = math.rad(self.species.branch_angle/2)
    local min_a = math.rad(360-self.species.branch_angle/2)
    if self.angle < math.pi and self.angle > 0 then
        self.side = 1
        if self.angle > math.rad(self.species.leaf_angle) then
            self.angle = math.rad(self.species.leaf_angle)
        end
    else
        self.side = -1
        if self.angle < math.pi*2 - math.rad(self.species.leaf_angle) then
            self.angle = math.pi*2 - math.rad(self.species.leaf_angle)
        end
    end


    --~ if self.angle > max_a and self.angle < math.pi then
        --~ self.angle  =   max_a
    --~ elseif self.angle < min_a and self.angle > math.pi then
        --~ self.angle  =   min_a
    --~ end
end

function GhostLeaf:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(self:getDrawable())

end

function GhostLeaf:getDrawable()
    return self.image, self.x_pos, self.y_pos, self.angle, self.side, 1, self.anchor.x, self.anchor.y
--~ --~
end

function GhostLeaf:getTree()
    return self.parent:getTree()
end

function GhostLeaf:confirm()
    print("Leaf confirmed")
    self.parent:confirmLeaf(self)
    self.dead   =   true
end

function GhostLeaf:cancel()
    print("Leaf cancelled")
    local t = self:getTree()
    t.appending_element =   nil
    t.appending         =   false
    self.dead   =   true

end
