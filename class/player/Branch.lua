------------------------------------------------------------------------
-- @class Branch
-- Class representing a player's branch
-- Branch of the player, can grow or or decay or do whatever branches do
------------------------------------------------------------------------

class "Branch"{
}

function Branch:__init__(p_parent, p_hook)
    print("new branch")
    self.parent     =   p_parent
    self.species    =   p_parent.species
    self.image      =   self.species.branch_image[1]
    self.empty_image=   self.species.branch_image_empty
    self.anchor     =   {}
    self.hook       =   {}
    self.center     =   {}
    self.branches   =   {}
    self.leafs      =   {}
    self.level      =   0
    self.angle      =   0
    self.time       =   0
    self.opacity    =   255
    self.selected   =   false
    self.transparent=   self.parent.transparent or false
    self.hook.x, self.hook.y        =   self.species.branch_hook[1].x, self.species.branch_hook[1].y
    self.anchor.x, self.anchor.y    =   self.species.branch_anchor[1].x, self.species.branch_anchor[1].y
    self.x_pos, self.y_pos          =   0, 0
    if p_hook then
        self.x_pos, self.y_pos      =   p_hook.x, p_hook.y --if several hooks, get the good one
    else
        self.x_pos, self.y_pos      =   p_parent.hook.x, p_parent.hook.y
    end
    self:initSap()
end

function Branch:setTransparent(p_bool)
    self.transparent    =   p_bool

end

function Branch:initSap()
       -- hard coded
   local sprite =   love.graphics.newImage("res/images/sprites/sap_bubbles.png")
   local sap    =   love.graphics.newParticleSystem( sprite, 3.5 )
   sap:setPosition( self.x_pos, self.y_pos  )
   sap:setOffset( 16, 16 )
   sap:setBufferSize( 2000 )
   sap:setEmissionRate( 6 )
   sap:setLifetime( -1 )
   sap:setParticleLife( 10 )
   sap:setColors( 70, 170, 70, 105, 30, 115, 30, 123 )
   sap:setSizes( 3, 3, 3 )
   sap:setSpeed( 40, 45  )
   sap:setDirection( self.angle - math.pi/2 )
   sap:setSpread( math.rad(0) )
   sap:setGravity( 0, 0 )
   sap:setRotation( math.rad(0), math.rad(0) )
   sap:setSpin( math.rad(2.5), math.rad(90), 1 )
   sap:setRadialAcceleration( 0 )
   sap:setTangentialAcceleration( 0 )
   self.sap     =   sap
   self.sap:start()

end

function Branch:update(dt)
    self.time   =   self.time + dt
    if self.transparent then
        self.opacity    =   self.opacity - 500 * dt
    else
        self.opacity    =   self.opacity + 500 * dt
    end
    if self.sap then self.sap:update(dt) end

    if self.opacity > 255 then self.opacity = 255 end
    if self.opacity < 0 then self.opacity = 0 end

    -- Resources uptake
    local nutriment =   -self:uptakeNutriment(dt)
    local water     =   -self:uptakeWater(dt)
    local light     =   self:collectLight(dt) - self:uptakeLight(dt)
    self:getTree().resources.nutriment.current  =   self:getTree().resources.nutriment.current  + nutriment
    self:getTree().resources.water.current      =   self:getTree().resources.water.current      + water
    self:getTree().resources.light.current      =   self:getTree().resources.light.current      + light
end

function Branch:collectLight( dt )
    if self.level == 0 then
        return self.species.branch_collect.light * dt
    else
        return 0
    end
end

function Branch:uptakeNutriment(dt)
    return self.species.branch_uptake.nutriment * dt
end

function Branch:uptakeWater(dt)
    return self.species.branch_uptake.water * dt
end

function Branch:uptakeLight(dt)
    return self.species.branch_uptake.light * dt
end

function Branch:levelUp()
    self.level  =   self.level + 1
    local img_number	=	clamp(self.level+1, 1, #self.species.branch_image )
    self.image	=	self.species.branch_image[img_number]
    self.anchor	=	self.species.branch_anchor[img_number]
    self.hook	=	self.species.branch_hook[img_number]
    self.parent:levelUp()
end

function Branch:confirmBranch(p_ghost)
    local t = self:getTree()
    if t:buyBranch() then
        local b = Branch(self)
        b:fromGhost(p_ghost)
        table.insert(self.branches, b)
        table.insert(self:getTree().branches, 1, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        -- If max reached: remove option
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        self:levelUp()
        return true
    else
        t:cancel()
        return false
    end
end

function Branch:confirmLeaf(p_ghost)

    local t = self:getTree()
    if t:buyLeaf() then
        local b = Leaf(self)
        b:fromGhost(p_ghost)
        table.insert(self.leafs, b)
    table.insert(self:getTree().leafs, 1, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        return true
    else
        t:cancel()
        return false
    end
end

function Branch:draw()
    love.graphics.setColor(255, 255, 255, 255)
    if self.selected then
        r,g,b = 200, 200, 200
    else
        r,g,b = 255, 255, 255
    end


    if self.opacity ~= 255 then
        love.graphics.draw(self:getDrawable(self.empty_image))
        if self.sap then
            love.graphics.draw(self.sap, 0, 0)
        end
    end
    --Drawing shell (sprite that
    if self.opacity ~= 0 then --If fully transparent do not draw
        love.graphics.setColor(r,g,b, self.opacity)
        love.graphics.draw(self:getDrawable())
    end

    if DEBUG then
        love.graphics.setColor(255,0,0)
        love.graphics.circle("fill",self.anchor.x, self.anchor.y, 10)
        love.graphics.setColor(0,255,0)
        love.graphics.circle("fill",self.x_pos, self.y_pos, 10)
        love.graphics.setColor(0,0,255)
        love.graphics.circle("fill", self.hook.x, self.hook.y, 10)
        love.graphics.setColor(255,255,255)
        love.graphics.circle("fill", self.center.x, self.center.y, 15)
    end
end

function Branch:fromGhost(p_ghost)
    self.anchor =   p_ghost.anchor
    self.x_pos  =   p_ghost.x_pos
    self.y_pos  =   p_ghost.y_pos
    self.angle  =   p_ghost.angle
    if self.sap then self.sap:setDirection( self.angle - math.pi/2 ) end
    local v_hook  =   vector(self.hook.x - self.anchor.x, self.hook.y - self.anchor.y)
    v_hook:rotate_inplace(self.angle)
    self.hook.x, self.hook.y        =   v_hook.x+self.x_pos, v_hook.y+self.y_pos
    self.center.x, self.center.y    =   v_hook.x/2+self.x_pos, v_hook.y/2+self.y_pos

end

function Branch:getDrawable(p_image)
    local scale =   1 + (self.level/self.species.branch_level_factor)
    if not p_image then

        return self.image, self.x_pos, self.y_pos, self.angle, scale, 1, self.anchor.x, self.anchor.y
    else
        return p_image, self.x_pos, self.y_pos, self.angle, scale, 1, self.anchor.x, self.anchor.y
    end
end

function Branch:getTree()
    return self.parent:getTree()
end

function Branch:appendBranch()
    local t = self:getTree()
    if t:canAffordBranch() then
        local b = GhostBranch(self)
        t.appending_element =   b
        t.appending         =   true
        t.resources.nutriment.preview   =   t.species.branch_cost.nutriment
        t.resources.water.preview       =   t.species.branch_cost.water
        t.resources.light.preview       =   t.species.branch_cost.light

        --Correct this
        print("Seed is inserting the dummy in GUI")
        table.insert(GAME_OBJECTS, b)
        print("Gui has now: "..#GUI)
        b=nil
        return true
    else
        print("Can't afford a branch yet")
        self.selected   =   false
        return false
    end

end

function Branch:appendLeaf()

    local t = self:getTree()
    if t:canAffordLeaf() then
        local b = GhostLeaf(self)
        t.appending_element =   b
        t.appending         =   true
        t.resources.nutriment.preview   =   t.species.leaf_cost.nutriment
        t.resources.water.preview       =   t.species.leaf_cost.water
        t.resources.light.preview       =   t.species.leaf_cost.light

        --Correct this
        table.insert(GAME_OBJECTS, b)
        b=nil
        return true
    else
        print("Can't afford a leaf yet")
        self.selected   =   false
        return false
    end

end

function Branch:getActions()
    local a = {}
    if #self.branches < self.species.max_branches then
        table.insert(a, "Branch")
    end

    if #self.leafs < self.species.max_leafs then
        table.insert(a, "Leaf")
    end
    
    table.insert(a, "Inspect")
    return a

end

function Branch:testPoint(p_x, p_y)
    local test = point_vs_raabb (p_x, p_y, self.center.x, self.center.y , self.image:getWidth()/2, self.image:getHeight()/2, self.angle - math.pi)
    return test

end

function Branch:getToolTipInfo()
	--~ local text = self.species.branch_description .. "\n"
	local text = ""
	text = text .. _("Collecting") .. "\n"
    if self.level == 0 then
        text = text .. _("light") .. " : " .. self.species.branch_collect.light .. "\n"
    else
        text = text .. _("light") .. " : 0 \n"
    end
	text = text .. _("Uptaking") .. "\n"
	text = text .. _("nutriments") .. " : " .. self.species.branch_uptake.nutriment .. "\n"
	text = text .. _("water") .. " : " .. self.species.branch_uptake.water .. "\n"
	text = text .. _("light") .. " : " .. self.species.branch_uptake.light .. "\n"
	return text
end

function Branch:inspect()
	local tt = Tooltip( self, self.center, self:getToolTipInfo() )
	return tt
end
