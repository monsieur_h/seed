------------------------------------------------------------------------
-- @class Seed
-- Class representing a seed, that is to say, a player-to-be
-- Can jump lightly and give birth to a Core
------------------------------------------------------------------------
class "Seed"{
    x_pos   =   0,
    y_pos   =   0
}


------------------------------------------------------------------------
-- Function to init a seed anywhere
-- @param p_x x position in the world
-- @param p_y y position in the world
-- @param p_world (optionnal) physical world
------------------------------------------------------------------------
function Seed:__init__(p_x, p_y, p_world)
    self.x_pos  =   p_x
    self.y_pos  =   p_y
    self.angle  =   0
    self.image  =   love.graphics.newImage("res/images/sprites/oak.png") -- Default image
    self.jumps  =   2
    self.time   =   0
    self.level  =   0
    self.branches       =   {}
    self.roots          =   {}
    self.last_jump      =   0
    self.plant_duration =   0.5 -- Duration of the planting effect
    self.planted        =   false
    self.actions        =   {"Plant", "Jump Left", "Jump Right"}
    self.parent         =   nil
    self.selected       =   false
    --Particles
    flare = love.graphics.newImage("res/images/sprites/flare.png")

    self.creation_effect =   love.graphics.newParticleSystem(flare, 1000)
    self.creation_effect:setEmissionRate(500)
    self.creation_effect:setSpeed(200, 400)
    self.creation_effect:setSizes(1, 2)
    self.creation_effect:setColors(230, 255, 230, 255, 128, 255, 128, 0)
    self.creation_effect:setPosition(400, 300)
    self.creation_effect:setLifetime(1.3)
    self.creation_effect:setParticleLife(1.3)
    self.creation_effect:setDirection(0)
    self.creation_effect:setSpread(360)
    self.creation_effect:setTangentialAcceleration(200)
    self.creation_effect:setRadialAcceleration(-800)
    self.creation_effect:stop()


    self.jump_effect     =   love.graphics.newParticleSystem(flare, 1000)
    self.jump_effect:setEmissionRate(20)
    self.jump_effect:setSpeed(30, 40)
    self.jump_effect:setSizes(1, 2)
    self.jump_effect:setColors(230, 255, 230, 255, 128, 255, 128, 0)
    self.jump_effect:setPosition(400, 300)
    self.jump_effect:setLifetime(0.5)
    self.jump_effect:setParticleLife(0.5)
    self.jump_effect:setDirection(0)
    self.jump_effect:setSpread(180)
    self.jump_effect:setTangentialAcceleration(2)
    self.jump_effect:setRadialAcceleration(-8)
    self.jump_effect:stop()

    --Physics?
    if p_world then
        self:setWorld(p_world)
    end
end

function Seed:getActions()
    return self.actions
end

function Seed:setEffectDuration(p_time)
    if p_time then self.plant_duration =   p_time end

end

function Seed:setWorld(p_world)
        self.world      =   p_world
        center_y        =   self.image:getWidth()  / 2 + self.y_pos
        center_x        =   self.image:getHeight() / 2 + self.x_pos
        self.body       =   love.physics.newBody( p_world, center_x, center_y, "dynamic" )
        self.fixture    =   love.physics.newFixture(self.body, self.shape, 1)
        self.fixture:setRestitution(0.15)
        self.fixture:setFriction(0.95)
        --~ self.body:setMass(2)
        self.body:setLinearDamping(0.1)
        self.body:getAngularDamping(0.1)
end

function Seed:update(p_dt)
    self.time   =   self.time + p_dt
    if self.body then
        self.x_pos  =   self.body:getX()
        self.y_pos  =   self.body:getY()
        self.angle  =   self.body:getAngle()
    end
end

function Seed:levelUp()
    self.level  =   self.level + 1
end

function Seed:draw()
    if self.selected then
        love.graphics.setColor(200, 200, 200)
    else
        love.graphics.setColor(255, 255, 255)
    end
    love.graphics.draw(self:getDrawable())

    if DEBUG then
        if self.hook then
            love.graphics.setColor(0,0,255)
            love.graphics.circle("fill",self.hook.x, self.hook.y, 10)
        end
        love.graphics.setColor(0,255,0)
        love.graphics.circle("fill", self.x_pos, self.y_pos, 10)
        love.graphics.setColor(0,255, 255)
        local x, y = self.body:getWorldCenter()
        love.graphics.circle("fill", x, y, 10)
    end

end

function Seed:getDrawable()
    if self.circleShape then
        return self.image, self.x_pos, self.y_pos, self.angle, 1, 1, self.image:getWidth()/2, self.image:getHeight()/2
    else
        return self.image, self.x_pos, self.y_pos, self.angle, 1, 1
    end
end

function Seed:setJumpLeft(p_number)
    if p_number then
        self.jumps  =   p_number
    end
end

function Seed:jump(p_direction)
    if self.jumping then return end
    if self.jumps <= 0 then return end
    if self.time - self.last_jump <= 0.5 then return end

    self.last_jump = self.time
    self.jump_effect:setPosition(self.x_pos, self.y_pos)
    self.jump_effect:start()
    if not p_direction then
        p_direction = "right"
    end

    if p_direction == "right" then
        self.body:applyLinearImpulse( 75, -75 )
    else
        self.body:applyLinearImpulse( -75, -75 )
    end
    self.jumps   =   self.jumps - 1

end

function Seed:setShape(p_shape)
    if p_shape then
        self.shape      =   love.physics.newPolygonShape(unpack(p_shape))
    else
        self.circleShape=   true
        self.shape      =   love.physics.newCircleShape(self.image:getWidth() / 3) --the ball's shape has a radius of 20
    end
end

function Seed:plant()
    self.hook   =   {}
    self.hook.x, self.hook.y =   self.body:getWorldCenter()

    if not self.planted then
        self.planted    =   true
        self.plant_time =   self.time
        self.creation_effect:setPosition(self.body:getWorldCenter())
        self.creation_effect:start()
        self.body:setType("static")
        self.actions    =   nil
        self.actions    =   {}
        table.insert(self.actions, "Branch")
        table.insert(self.actions, "Root")
    end
end

function Seed:testPoint(p_x, p_y)
    return self.fixture:testPoint(p_x, p_y)
end

function Seed:setImage(p_img)
    self.image  =   p_img

end

function Seed:setPos(p_x, p_y)
    if p_x then
        self.x_pos  =   p_x
    end
    if p_y then
        self.y_pos  =   p_y
    end

end

function Seed:getJumps()
    return self.jumps

end

function Seed:getTree()
    if self.parent then return self.parent:getTree() else return nil end
end

function Seed:isPlaced()
    if self.planted then
        return ( (self.time - self.plant_time) > self.plant_duration)
    else
        return false
    end

end

function Seed:appendBranch()
    local t = self:getTree()
    if t:canAffordBranch() then
        local b = GhostBranch(self)
        t.appending_element =   b
        t.appending         =   true
        t.resources.nutriment.preview   =   t.species.branch_cost.nutriment
        t.resources.water.preview       =   t.species.branch_cost.water
        t.resources.light.preview       =   t.species.branch_cost.light

        --Correct this
        print("Seed is inserting the dummy in GUI")
        table.insert(GAME_OBJECTS, b)
        print("Gui has now: "..#GUI)
        b=nil
        return true
    else
        print("Can't afford a branch yet")
        self.selected   =   false
        return false
    end
end

function Seed:confirmBranch(p_ghost)
    local t = self:getTree()
    if t:buyBranch() then
        local b = Branch(self)
        b:fromGhost(p_ghost)
        table.insert(self.branches, b)
        table.insert(self:getTree().branches,0, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        -- If max reached: remove option
        if #self.branches == self.species.max_branches then
            for i, v in pairs(self.actions) do
                if v == "Branch" then table.remove(self.actions, i) end
            end
        end
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        return true
    else
        t:cancel()
        return false
    end
end


function Seed:appendRoot()
    local t = self:getTree()
    if t:canAffordRoot() then
        local b = GhostRoot(self)

        t.appending_element =   b
        t.appending         =   true


        t.resources.nutriment.preview   =   t.species.root_cost.nutriment
        t.resources.water.preview       =   t.species.root_cost.water
        t.resources.light.preview       =   t.species.root_cost.light

        --Correct this
        print("Seed is inserting the dummy root in GUI")
        table.insert(GAME_OBJECTS, b)
        print("Gui has now: "..#GUI)
        b=nil
        return true
    else
        self.selected   =   false
        return false
    end
end

function Seed:confirmRoot(p_ghost)
    local b = Root(self)
    local t = self:getTree()
    if t:buyRoot() then
        b:fromGhost(p_ghost)
        table.insert(self.roots, b)
        table.insert(self:getTree().roots, 0, b)
        print("GameObject has now: "..#GAME_OBJECTS)
        -- If max reached: remove option
        if #self.roots == self.species.max_roots then
            for i, v in pairs(self.actions) do
                if v == "Root" then table.remove(self.actions, i) end
            end
        end
        t.resources.nutriment.preview   =   0
        t.resources.water.preview       =   0
        t.resources.light.preview       =   0
        return true
    else
        t:cancel()
        return false
    end
end
