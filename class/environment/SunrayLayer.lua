------------------------------------------------------------------------
-- @class SunrayLayer
-- Class representing a layer of sunrays
------------------------------------------------------------------------


class "SunrayLayer"{
}

function SunrayLayer:__init__()
    self.pos        =   { x={min=0,max=love.graphics.getWidth()}, y={min=-love.graphics.getHeight(), max=love.graphics.getHeight()} }
    self.rayNumber  =   2
    self.duration   =   1.0 --in seconds
    self.rays       =   {}
    self.images     =   {}
end

function SunrayLayer:update( dt )
    for i, s in pairs( self.rays ) do
        s:update( dt )
        if not s:isAlive() then
            table.remove( self.rays, i )
        end
    end

    -- Let's cast new sunrays !!
    while ( #self.rays < self.rayNumber ) do
        self:generateRay()
    end
end

function SunrayLayer:generateRay()
    local s = Sunray()
    local img_n =   math.random( 1, #self.images )
    s:setImage( self.images[img_n] )
    --s:setBounds( self.pos )
    table.insert( self.rays, s )
end

function SunrayLayer:draw( p_alpha )
    for i,v in pairs( self.rays ) do --Update the rays
        v:draw( p_alpha or nil )
    end
end

function SunrayLayer:addImage( p_image )
    assert( p_image )
    table.insert( self.images, p_image )
end

function SunrayLayer:loadImage( p_path )
    assert( p_path )
    -- Finding a random image of sunray
    local image = love.graphics.newImage( p_path )
    self:addImage( image )

    -- Making random speed
    local speed =   {}
    speed.x     =   math.random(0, 10)
    speed.y     =   0

    -- Random pos as well...
    local position  =   {}
    position.x      =   math.random( self.pos.x.min, self.pos.x.max )
    position.y      =   math.random( self.pos.y.min, self.pos.y.max )

    local newRay    =   Sunray()
    newRay.speed    =   speed
    newRay.pos      =   position
    newRay:setImage( image )
    table.insert( self.rays, newRay )
end
