------------------------------------------------------------------------
-- @class Cloud
-- Class representing a cloud
------------------------------------------------------------------------

class "Cloud"{
}

function Cloud:__init__()
    self.pos    =   {x=0, y=0}
    self.speed  =   {x=10, y=0}
    self.image  =   nil
    self.offset =   {x=0, y=0}
    self.scale  =   1
end


function Cloud:update( dt )
    self.pos.x  =   self.pos.x  + (self.speed.x * dt)
end

function Cloud:draw()
    --Color is handled by the CloudLayer calling the draw to minimize calls to setColor()
    love.graphics.draw( self.image, self.pos.x, self.pos.y, 0, self.scale, self.scale, self.offset.x, self.offset.y )
    if DEBUG then
        love.graphics.circle( "fill" , self.pos.x, self.pos.y, 32 )
    end
end

function Cloud:setImage( p_image )
    assert( p_image )
    self.image  =   p_image
    self.offset.x   =   p_image:getWidth() / 2
    self.offset.y   =   p_image:getHeight() / 2
end
