------------------------------------------------------------------------
-- @class Sun
-- Class representing the sun
------------------------------------------------------------------------


class "Sun"{
}

function Sun:__init__()
    self.time       =   0
    self.distance   =   600
    self.pos        =   {x=0, y=0}
    table.insert(SURFACE_OBJECT, self)
end


function Sun:draw( p_alpha )
    love.graphics.setColor( p_alpha or 255, p_alpha or 255, p_alpha or 255, 255)
    love.graphics.setLine(1)
    local sinm = math.sin( self.time * 80 )
    local sunr = 30 + sinm * 0.75 -- add some life to the sun's size
    love.graphics.circle( "fill", self.pos.x, self.pos.y, sunr + 0.5, 64)
    love.graphics.circle( "line", self.pos.x, self.pos.y, sunr, 64)
    local passes = 5
    for i=1, passes do
        love.graphics.setColor( p_alpha or 255, p_alpha or 255, p_alpha or 255, passes - i / passes * 255)
        love.graphics.circle( "line", self.pos.x, self.pos.y, sunr + i, 64)
    end
end

function Sun:update(dt)
    self.time   =   self.time+dt

end

function Sun:setPos(p_x, p_y)
    self.pos    =   {x=p_x, y=p_y}

end
