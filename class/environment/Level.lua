class "Level"{
}

function Level:__init__(filename, p_world)
    assert( filename, "No level file to load" )
    -- load table from file
    local info,err = table.loveload( filename )
    assert( err == nil, err )
    info.imagename  =   info.imagename:match( "([^/]+)$" ) or info.imagename
    self.image      =   love.graphics.newImage( "res/images/level/"..info.imagename )
    self.background =   love.graphics.newImage( "res/images/level/"..info.background )
    print("loading ".. "res/images/level/"..info.background)
    self.pos        =   {x=0, y=0}
    self.polygon    =   info.collidable or nil
    self.height     =   self.image:getHeight()
    self.width      =   self.image:getWidth()

    --Generating random nutriments
    self.nutriments =   {}
    nut = nil
    for i = 0, 200, 1 do
        local nut   =   Nutriment()
        local x     =   math.random(self.polygon[1], self.polygon[#self.polygon - 1])
        local y     =   math.random(0, self.height)

        table.insert(ENV_OBJECTS, nut)
        while self:isUnderground(x, y) do
            x   =   math.random(self.polygon[1], self.polygon[#self.polygon - 1])
            y   =   math.random(0, self.height)
        end
        nut:setPosition(x, y)
        table.insert(self.nutriments, nut)
    end

    self.waterspots =   {}
    --Reading waterspots from file
    for i, v in pairs(info.resources.water) do
        local w =   WaterSource()
        w:setPosition(v.x, v.y)
        table.insert(self.waterspots, w)

    end

    --If a world is passed, we go for a physics simulation
    if p_world then
        self.world  =   p_world
        levelshape  =   love.physics.newChainShape( false, unpack(info.collidable) )
        levelbody   =   love.physics.newBody(p_world, 0,0, "static")
        levelfixt   =   love.physics.newFixture(levelbody, levelshape)
        --debug?
        self.shape  =   levelshape
        self.body   =   levelbody
    end
end

function Level:update(p_dt)
    --~ TODO:Ground:update?
end

function Level:getDrawable()
    return self.image, self.pos.x, self.pos.y
end

function Level:draw( p_alpha )
    love.graphics.setColor( p_alpha or 255, p_alpha or 255, p_alpha or 255, 255 )
    self:drawBackground()
    love.graphics.draw(self:getDrawable())
    -- Drawing a mirror image left and right of it
    love.graphics.draw( self.image, self.pos.x - self.image:getWidth(), self.pos.y )
    love.graphics.draw( self.image, self.pos.x + self.image:getWidth(), self.pos.y )
    for i, v in pairs(self.nutriments) do
        v:draw( p_alpha )
    end

    for i, v in pairs(self.waterspots) do
        v:draw( p_alpha )
    end

    if DEBUG then
        love.graphics.setLine( 10 )
        love.graphics.rectangle( "line",  self.pos.x, self.pos.y, self.pos.x+self.width, self.pos.y+self.height )
    end

end

function Level:drawBackground()
    love.graphics.draw( self.background, self.pos.x, self.pos.y, 0, 1, 1, 0, self.background:getHeight() / 2  )
    --TODO: change that -> mirror of the background to left and right position of the image-- Drawing a mirror image left and right of it
end
-- Returns true if the point is underground
function Level:isUnderground(p_x, p_y)
    --Find closest segment
    for i=1, #self.polygon, 2 do
        if self.polygon[i] >= p_x then -- Finding the good point of the surface
            local horizon    =   self.polygon[i+1] -- Getting Y value of the ground here...
            if i>2 and horizon > self.polygon[i-1] then -- Testing wich is the lowest (Y) with the previous point
                horizon      =   self.polygon[i-1]
            end

            return (p_y < horizon)
        end
    end
end

function Level:isInBounds( p_x, p_y )
    assert( p_x  and p_y )

    return ( p_x > self.pos.x
        and p_x < (self.level.pos.x + self.level.width)
        and p_y < (self.level.pos.y + self.level.height) )
end
