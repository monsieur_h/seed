------------------------------------------------------------------------
-- @class Nutriment
-- Class representing a rich underground nutriment zone
------------------------------------------------------------------------

class "Nutriment"{
}

function Nutriment:__init__()
    self.x_pos, self.y_pos  =   0, 0
    local r                 =   math.random(50, 75)
    self.resource           =   {max=r, current=r}
    self.dead               =   false
    self.selected           =   false
    self.hovered            =   false
    self.time               =   0
    self.selectedSince      =   nil
    self.glowingTime        =   0.5--In seconds

end

function Nutriment:setPosition(p_x, p_y)
    self.x_pos  =   p_x or 0
    self.y_pos  =   p_y or 0
    self.pos    =   {x=p_x, y=p_y}

end

function Nutriment:update(dt)
    self.time   =   self.time + dt
    if self.resource.current <= 0 then
        self.dead    =    false
    end
end

function Nutriment:draw( p_alpha )
    local ratio     =   self.resource.current / self.resource.max
    local color     =   255 - ratio * 255
    local size      =   self.resource.max * ratio
    if p_alpha then
        alpha = 90 * p_alpha / 255
    end

    --If just selected, make it glow...
    if self.selected and (self.time - self.selectedSince) < self.glowingTime then
        local ratio =   (self.time - self.selectedSince) / self.glowingTime
        love.graphics.setColor( 255, 255 ,255 ,200 - ratio * 200 )
        love.graphics.circle( "fill", self.x_pos, self.y_pos, 50 * ratio )--Make the effect change during time
    end

    --Drawing the nutriment
    love.graphics.setColor(255, color, color, alpha or 90 )
    love.graphics.circle("fill", self.x_pos, self.y_pos, 5 )
end

function Nutriment:collect( dt )
    local delta = 1 * dt
    self.resource.current = self.resource.current - delta
    return delta
end

function Nutriment:connect()
    self.selected   =   true
    self.selectedSince  =   self.time
end
