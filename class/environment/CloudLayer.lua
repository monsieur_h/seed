------------------------------------------------------------------------
-- @class CloudLayer
-- Class representing a cloud layer in the sky
------------------------------------------------------------------------

class "CloudLayer"{
}

function CloudLayer:__init__()
    self.pos    =   {x={min=0, max=1024}, y={min=0, max=768}}
    self.speed  =   {min=10, max=50}
    self.cloudNumber    =   3
    self.clouds =   {}
    self.images =   {}
end

function CloudLayer:setImage(p_image)
    assert(p_image, "Cloud layer needs an image")
    self.image  =   p_image
end

function CloudLayer:update( dt )
    --~ self.pos.x  =   self.pos.x + dt * self.speed --Use this in a cloud now
    --update all clouds
    for i,c in ipairs( self.clouds ) do
        c:update( dt )
        if self:isOutOfBound( c.pos.x, c.pos.y ) then --removing clouds out of screen
            table.remove( self.clouds, i )
        end
    end

    --create new clouds if necessary
    while ( #self.clouds < self.cloudNumber ) do
        self:generateCloud()
    end
end

function CloudLayer:draw()
    --~ love.graphics.draw(self.image, self.pos.x, self.pos.y)
    for i, c in ipairs( self.clouds ) do
        c:draw()
    end
end

function CloudLayer:loadImage( p_path )
    assert( p_path )
    local image = love.graphics.newImage( p_path )
    self:addImage( image )
end

function CloudLayer:addImage( p_image )
    assert( p_image )
    table.insert( self.images, p_image )
end

function CloudLayer:generateCloud()
    local c =   Cloud()
    local img_n =   math.random( 1, #self.images )
    c:setImage( self.images[img_n] )
    c.speed.x   =   math.random( self.speed.min, self.speed.max )
    c.speed.y   =   0
    c.pos.x =   math.random( self.pos.x.min, self.pos.x.max )
    c.pos.y =   math.random( self.pos.y.min, self.pos.y.max )
    table.insert( self.clouds, c )
end

function CloudLayer:isOutOfBound( p_x, p_y )
    assert( p_x and p_y )
    return ( p_x > self.pos.x.max
        or p_x < self.pos.x.min
        or p_y > self.pos.y.max
        or p_y < self.pos.y.min )

end
