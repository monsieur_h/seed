------------------------------------------------------------------------
-- @class Sunray
-- Class representing a single sunray
------------------------------------------------------------------------


class "Sunray"{
}

function Sunray:__init__( p_image )
    self.pos        =   {x=0, y=0}
    self.image      =   p_image or nil
    self.movement   =   {x=math.random(-10, 10), y=0}
    self.time       =   0
    self.dead       =   false
    self.duration   =   1
    self.bounds     =   nil
    print("new sunray!")
end

function Sunray:setImage( p_image )
    assert( p_image )
    self.image  =   p_image
end

function Sunray:update( dt )
    self.time   =   self.time + dt
    if self.time > self.duration then self.dead = true end
    self.pos.x  =   self.pos.x + self.movement.x
    self.pos.y  =   self.pos.y + self.movement.y
end

function Sunray:draw( p_alpha )
    love.graphics.setColor( 255 or alpha, 255 or alpha, 255 or alpha, 255 )
    love.graphics.draw( self.image, self.pos.x, self.pos.y )
end

function Sunray:isAlive()
    return not self.dead
end
