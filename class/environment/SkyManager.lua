------------------------------------------------------------------------
-- @class SkyManager
-- Class representing the sky, with time and weather
-- Its skydome is centered and rotates slowly to show alternatively night and day
------------------------------------------------------------------------


class "SkyManager"{
}

function SkyManager:__init__(p_image, p_level)
    assert(p_image, "Trying to build skymanager without a skydome image")
    self.skydome    =   p_image
    self.time       =   0
    self.pos        =   {}  --Center at mid-bottom of screen
    self.pos.x      =   love.graphics.getWidth()/2
    self.pos.y      =   love.graphics.getHeight()
    self.offset     =   self.skydome:getWidth()/2
    self.angle      =   0
    self.level      =   p_level or nil
    self.clouds     =   {}
    self.sunrays    =   {}


    local w         =   love.graphics.getWidth() * 2
    local h         =   love.graphics.getHeight()
    local hypo      =   math.sqrt(((w*w) + (h*h))) --Hypotenuse of the triangle a.k.a
    self.scale      =   hypo / p_image:getWidth()
    self.scale      =   self.scale
    print("Scale of skydome :"..self.scale)
    self.timefactor =   0.02

    --Sun
    --self.sun        =   Sun()

    --Sunrays
    local layer     =   SunrayLayer()
    layer:loadImage( "res/images/level/sunray_01.png" )
    layer:loadImage( "res/images/level/sunray_02.png" )
    layer:loadImage( "res/images/level/sunray_03.png" )
    table.insert( self.sunrays, layer )

    --Clouds
    local cloud     =   CloudLayer()
    cloud:loadImage( "res/images/level/clouds_01.png" )
    cloud:loadImage( "res/images/level/clouds_02.png" )
    cloud:loadImage( "res/images/level/clouds_03.png" )
    cloud:loadImage( "res/images/level/clouds_04.png" )
    if self.level then
        cloud.pos.x.min         =   self.level.pos.x - self.level.width / 2
        cloud.pos.x.max         =   self.level.pos.x + self.level.width * 1.5

        cloud.pos.y.min         =   self.level.pos.y - self.level.height
        cloud.pos.y.max         =   self.level.pos.y - self.level.height / 10
    end
    cloud.cloudNumber   =   10
    table.insert(self.clouds, cloud)

end

function SkyManager:update(dt)
    self.time   =   self.time + dt
    self.angle  =   self.angle+ dt * self.timefactor

    -- Updating sunpos
    if self.sun then
        local rotation_center   =   vector(love.graphics.getWidth()/2, love.graphics.getHeight())
        local a = self.angle-math.pi/2
        local cd = math.cos ( a )
        local sd = math.sin ( a )
        sunx, suny = rotation_center.x, rotation_center.y
        sunx, suny =  cd* self.sun.distance + sunx, sd*self.sun.distance + suny

        self.sun:update(dt)
        self.sun:setPos(sunx, suny)
    end

    for i, layer in pairs(self.clouds) do --updating every layer
        layer:update( dt )
    end

    for i, ray in pairs( self.sunrays ) do --updating every layer of sunray
        ray:update( dt )
    end
end

function SkyManager:draw( p_alpha )
    --~ love.graphics.draw(sky, love.graphics.getWidth()/2 - sky:getWidth()/2, love.graphics.getHeight() - sky:getHeight()/2)
    love.graphics.setColor( p_alpha or 255, p_alpha or 255, p_alpha or 255, 255 )
    love.graphics.draw( self.skydome, self.pos.x, self.pos.y, self.angle, self.scale, self.scale, self.offset, self.offset )

    --Drawing sun
    if self.sun then
        self.sun:draw( p_alpha )
    end

    if DEBUG then
        --~ local tmp = 400-love.graphics.getHeight()
        --~ local abs_sunpos        =   vector(self.pos.x + 1000 - self.offset/2, self.pos.y + tmp - self.offset/2)
        --~ local rotation_center   =   vector(love.graphics.getWidth()/2, love.graphics.getHeight())
        --~ local a = self.angle-math.pi/2
        --~ local cd = math.cos ( a )
        --~ local radius = 600
        --~ local sd = math.sin ( a )
        --~ x, y = rotation_center.x, rotation_center.y
        --~ x, y =  cd* radius + x, sd*radius + y
        --~ local v_hook            =   rotation_center
        --~
        --~ local v_hook    =   vector(abs_sunpos.x - rotation_center.x, abs_sunpos.y - rotation_center.y)
        --~ v_hook:rotate_inplace(self.angle-math.pi/2)
        --~ v_hook:normalize_inplace()
        --~ local x, y              =   v_hook.x + rotation_center.x, v_hook.y + rotation_center.y
        --~ print ("sun pos: "..x ..", "..y)

        --~ love.graphics.setColor(255, 80, 80, 255)
        --~ love.graphics.circle( "fill", x, y, 50)
        --~ love.graphics.line( rotation_center.x, rotation_center.y, x, y)
    end
end

function SkyManager:drawClouds( p_alpha )
    if self.cam then
        local currentz = math.floor(self.cam.zoom * 100) / 100
        currentz = currentz - self.cam.u_zoom.min
        local range =   self.cam.u_zoom.max - self.cam.u_zoom.min
        local ratio =   currentz / range
        local alpha =   235 - (180 * ratio)
        if p_alpha then alpha = (p_alpha / 255) * alpha end --Forces alpha to follow the screen transition
        love.graphics.setColor( 255, 255, 255, alpha )
    else
        love.graphics.setColor( 255, 255, 255, 255 )
    end

    for i, layer in pairs(self.clouds) do
        layer:draw()
    end

end

function SkyManager:isNight()
    return (self.angle > 0 and self.angle < math.pi)
end

function SkyManager:isDay()
    return (not self:isNight() )
end

function SkyManager:setCam( p_cam )
    assert( p_cam )
    self.cam    =   p_cam
end

function SkyManager:isNight()
    local actualAngle   =   self.angle % (math.pi*2)
    return ( actualAngle > math.pi/2 and actualAngle < 3 * math.pi/2 )
end

function SkyManager:getTimeOfDay()
    local trueAngle =   self.angle % (math.pi*2)
    trueAngle       =   math.pi + trueAngle
    return trueAngle / (math.pi*2)
end

function SkyManager:drawSunrays( p_alpha )
    for i, ray in pairs( self.sunrays ) do
        ray:draw( p_alpha or nil )
    end
end
