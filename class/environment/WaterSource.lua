------------------------------------------------------------------------
-- @class WaterSource
-- Class representing an underground water spot
-- Contains branches and roots. Can append/decay them and make fruits
------------------------------------------------------------------------

class "WaterSource"{
}

--Loaded here for static purposes
groundwater_image   =   love.graphics.newImage("res/images/level/groundwater.png")

function WaterSource:__init__()
    self.pos        =   {}
    self.pos.x      =   0
    self.pos.y      =   0
    self.image      =   groundwater_image
    self.radius     =   self.image:getWidth()
    self.offset     =   self.radius/2
    self.resource   =   {current=50, max=50}
    self.dead       =   false
    self.selected   =   false

end


function WaterSource:update(dt)

end

function WaterSource:draw()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw( self:getDrawable() )
end

function WaterSource:getDrawable()
    return self.image, self.pos.x, self.pos.y, 0, 1, 1, self.offset, self.offset

end

function WaterSource:setPosition(p_x, p_y)
    assert(p_x, "Watersurce needs a X coord")
    assert(p_y, "Watersurce needs a Y coord")
    self.pos.x, self.pos.y  =   p_x, p_y

end
