------------------------------------------------------------------------
-- @class Button
-- Class representing a button
------------------------------------------------------------------------


class "Button"{
    x_pos   =   0,
    y_pos   =   0
}


function Button:__init__()
    self.x_pos  =   0
    self.y_pos  =   0

end

function Button:setImage(p_image)
    self.image  =   p_image
    self.x_pos2 =   self.x_pos + p_image:getWidth()
    self.y_pos2 =   self.y_pos + p_image:getHeight()

end

function Button:setLabel(p_label)
    self.label  =   p_label

end

function Button:setCenter(p_x, p_y)
    if self.image then
        height  =   self.image:getHeight()
        width   =   self.image:getWidth()
    else
        height  =   self.y_pos2 - self.y_pos
        width   =   self.x_pos2 - self.x_pos
    end
    self.x_pos  =   p_x - (width  / 2)
    self.y_pos  =   p_y - (height / 2)
    self.x_pos2 =   self.x_pos + width
    self.y_pos2 =   self.y_pos + height

end

function Button:setBounds(x_pos, y_pos, x_pos2, y_pos2)
    self.x_pos  =   p_x
    self.y_pos  =   p_y
    self.x_pos2 =   x_pos2
    self.y_pos2 =   y_pos2

end

function Button:getDrawable()

    return self.image, self.x_pos, self.y_pos

end


-- returns true if the point (p_x, p_y) is contained into the button
function Button:contains(p_x, p_y)
    if p_x > self.x_pos
        and p_x < self.x_pos2
        and p_y > self.y_pos
        and p_y <self.y_pos2
    then
        return true
    else
        return false
    end
end
