------------------------------------------------------------------------
-- @class RadialMenu
-- Class representing a menu around the mouse
------------------------------------------------------------------------

class "RadialMenu"{
}

function RadialMenu:__init__(p_target)
    assert(p_target, "A radial menu needs a target")

    self.items      =   {}
    self.actions    =   {}
    self.target     =   p_target
    self.radius     =   80
    self.image      =   love.graphics.newImage("res/images/gui/radial_item.png")
    self.time       =   0
    self.animating      =   true
    self.anim_duration  =   0.25
    self.hovereditem    =   0 -- indice of the hovered item (to animate/show label)
    self.anim_factor    =   0 -- goes from O to 1 over animation time
    self.alive          =   true
    self.target.selected    =   true
    if not clicksound then
        clicksound      =   love.audio.newSource("res/sounds/button_onClick.ogg", "static")
    end
end

function RadialMenu:update(dt)
    self.time           =   self.time + dt
    self.hovereditem    =   0
    if self.time < self.anim_duration then
        self.anim_factor    =   self.time / self.anim_duration
    else
        self.animating  =   false
    end
end

function RadialMenu:setPosition(p_x, p_y)
    self.x_pos, self.y_pos  =   p_x, p_y
end

function RadialMenu:setItems(p_items)
    if not p_items or #p_items == 0 then
        self.alive  =   false
        self.target.selected    =   false
    else
        self.items  =   p_items
    end
end

function RadialMenu:draw()
    love.graphics.setColor(255, 255, 255)
    love.graphics.circle("fill", self.x_pos, self.y_pos, 10)
    local angle_offset  =   (math.pi * 2) / #self.items
    for num, item in pairs(self.items) do
        local angle =   angle_offset * num
        local radius=   self.radius
        if self.animating then
            angle   =   angle + ( math.pi - (math.pi * self.anim_factor) )
            radius  =   self.radius * self.anim_factor
        end

        local x =   (radius * math.cos(angle)) + self.x_pos
        local y =   (radius * math.sin(angle)) + self.y_pos
        love.graphics.setColor(150, 150, 150, 150)
        love.graphics.setLine(3, "smooth")
        love.graphics.line(self.x_pos, self.y_pos, x, y)
        love.graphics.setColor(255, 255, 255, 230)
        love.graphics.draw(self.image, x, y, 1, 1, 1, self.image:getWidth()/2, self.image:getHeight()/2)
        if not self.animating and num==self.hovereditem then
            love.graphics.print(_(item), x, y)
        end

    end

end

function RadialMenu:testPoint(x, y)
    local point_in  =   false
    local angle_offset  =   (math.pi * 2) / #self.items
    for k,v in pairs(self.items) do
        local angle =   angle_offset * k
        local item_x =   (self.radius * math.cos(angle)) + self.x_pos
        local item_y =   (self.radius * math.sin(angle)) + self.y_pos
        local distance = math.sqrt((x-item_x)^2 + (y-item_y)^2)
        if distance < self.image:getWidth()/2 then
            point_in    =   true
        end
    end
    return point_in
end

-- Returns true/false if the click is handled
function RadialMenu:onClick(x,y)
    local click_handled =   false
    self.alive  =   false --I can't stand being clicked elsewhere, delete me pls
    local angle_offset  =   (math.pi * 2) / #self.items
    for k,item in pairs(self.items) do
        local angle =   angle_offset * k
        local item_x =   (self.radius * math.cos(angle)) + self.x_pos
        local item_y =   (self.radius * math.sin(angle)) + self.y_pos
        local distance = math.sqrt((x-item_x)^2 + (y-item_y)^2)
        if distance < self.image:getWidth()/2 then
            print("Clicked: "..item)
            if self.target then
                if item == "Plant" then
                    self.target:plant()

                elseif item == "Branch" then
                    self.target:appendBranch()

                elseif item== "Root" then
                    self.target:appendRoot()

                elseif item == "Leaf" then
                    self.target:appendLeaf()

                elseif item == "Jump Left" then
                    self.target:jump("left")

                elseif item == "Jump Right" then
                    self.target:jump("right")
                    
				elseif item == "Inspect" then
					local tooltip = self.target:inspect()
					table.insert( GUI, tooltip )
				
                end
            end
            love.audio.stop(clicksound)
            love.audio.play(clicksound)
            return true --click has been handled
        end
    end
    return click_handled
end

function RadialMenu:onHover(x,y)
    local angle_offset  =   (math.pi * 2) / #self.items
    for k,v in pairs(self.items) do
        local angle =   angle_offset * k
        local item_x =   (self.radius * math.cos(angle)) + self.x_pos
        local item_y =   (self.radius * math.sin(angle)) + self.y_pos
        local distance = math.sqrt((x-item_x)^2 + (y-item_y)^2)
        if distance < self.image:getWidth()/2 then
            self.hovereditem    =   k
        end
    end
end

function RadialMenu:isAlive()
    return self.alive
end
