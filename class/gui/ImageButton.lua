-- Util class ImageButton to make fading and noisy buttons
class "ImageButton"{
}

function ImageButton:__init__()
    self.hover      =   false
    self.alpha      =   0
    self.glow       =   0
    self.image      =   nil
    self.label      =   nil
    self.rect       =   nil
    self.fadeSpeed  =   400
    self.hoversound =   nil
    self.clicksound =   nil
    self.selected   =   false
    --~ self.value      =   nil

end

function ImageButton:loadImage(p_path)
    self.image  =   love.graphics.newImage(p_path)
    self.rect   =   Rectangle(0, 0, self.image:getWidth(), self.image:getHeight() )

end

function ImageButton:setImage(p_img)
    self.image  =   p_img
    self.rect   =   Rectangle(0, 0, self.image:getWidth(), self.image:getHeight() )

end

function ImageButton:loadLabelImage(p_path)
    self.label  =   love.graphics.newImage(p_path)

end

function ImageButton:setLabelImage(p_img)
    self.label  =   p_img

end

function ImageButton:loadIdleImage(p_path)
    self.idleImage  =   love.graphics.newImage(p_path)
    self.rect       =   Rectangle(0, 0, self.idleImage:getWidth(), self.idleImage:getHeight() )

end

function ImageButton:setIdleImage(p_img)
    self.idleImage  =   p_img
    self.rect       =   Rectangle(0, 0, self.idleImage:getWidth(), self.idleImage:getHeight() )
end

function ImageButton:loadSelectedImage(p_path)
    self.selectedImage  =   love.graphics.newImage(p_path)

end

function ImageButton:setSelectedImage(p_img)
    self.selectedImage  =   p_img

end

function ImageButton:loadHoverSound(p_path)
    self.hoversound  =   love.audio.newSource(p_path, "static")
    self.hoversound:stop()

end

function ImageButton:loadClickSound(p_path)
    self.clicksound  =  love.audio.newSource(p_path, "static")
    self.clicksound:stop()

end

function ImageButton:setHoverSound(p_sound)
    self.hoversound  =   p_sound
    self.hoversound:stop()
end

function ImageButton:setClickSound(p_sound)
    self.clicksound =   p_sound
    self.clicksound:stop()

end

function ImageButton:click()
    if self.clicksound then
        self.clicksound:stop()
        self.clicksound:play()
    end
end

function ImageButton:setCenter(p_x, p_y)
    self.rect:setCenter(p_x, p_y)

end

function ImageButton:update(dt)
    if self.hover then
        self.alpha      =   self.alpha + self.fadeSpeed * dt
        self.alpha      =   clamp(self.alpha, 0, 255)
        if self.hoversound and self.hoversound:isStopped() then
            self.hoversound:stop()
            --self.hoversound:play()
            self.hoversound:play()
        end
    else
        self.alpha      =   self.alpha - self.fadeSpeed * dt
        self.alpha      =   clamp(self.alpha, 0, 255)
        if self.hoversound then self.hoversound:stop() end
    end

    if self.selected then
        self.glow       =       self.glow + self.fadeSpeed * dt
        self.glow       =       clamp(self.glow, 0, 255)

    else
        self.glow       =       self.glow - self.fadeSpeed * dt
        self.glow       =       clamp(self.glow, 0, 255)
    end

    self.hover  =   false --reset hover state
end

function ImageButton:draw(p_max)
    p_max   =   p_max or 255
    --Displayed when Idle
    if self.idleImage then
        local a = math.min(p_max, 255 - (self.glow or 0) )
        love.graphics.setColor(255, 255, 255, a )
        love.graphics.draw(self.idleImage, self.rect.x, self.rect.y)
    end

    --Has alpha
    if self.alpha ~= 0 then
        love.graphics.setColor(255, 255, 255, math.min(self.alpha, p_max) )
        if self.image then
            love.graphics.draw(self.image, self.rect.x, self.rect.y)

        end
        if self.label then
            love.graphics.draw(self.label, self.rect.x, self.rect.y)
        end
    end

    --When selected
    if self.glow ~= 0 and self.selectedImage then
        love.graphics.setColor(255, 255, 255, math.min(self.glow, p_max) )
        love.graphics.draw(self.selectedImage, self.rect.x, self.rect.y)
    end

    --Value
    if self.value then
        love.graphics.setColor(255, 255, 255, math.min(p_max, self.alpha) )
        love.graphics.setFont( big_font )
        local offsety = big_font:getHeight() / 2
        local offsetx = big_font:getWidth(self.value)/2
        love.graphics.print( _(self.value), self.rect.center_x+self.rect.width/2-offsetx, self.rect.center_y - offsety )
    end


    if DEBUG then
        love.graphics.setColor(255, 230, 230, 230)
        self.rect:draw()
        if self.clickableZone then
            love.graphics.setColor(230, 255, 230, 230)
            self.clickableZone:draw()
        end
    end
end

function ImageButton:setClickableZone(p_width, p_height)
    assert(p_width)
    assert(p_height)
    self.clickableZone  =   Rectangle(0, 0, p_width, p_height)
    self.clickableZone:setCenter( self.rect.center_x, self.rect.center_y)

end

function ImageButton:contains(p_x, p_y)
    assert(p_x)
    assert(p_y)
    if self.clickableZone then
        return self.clickableZone:contains(p_x, p_y)
    elseif self.rect then
        return self.rect:contains(p_x, p_y)
    else
        return false
    end
end
