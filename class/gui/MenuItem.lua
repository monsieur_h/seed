------------------------------------------------------------------------
-- @class MenuItem
-- Class representing a clickable menu item
------------------------------------------------------------------------


class "MenuItem"{
}

function MenuItem:__init__(p_label, p_value)
    self.label  =   p_label or "No Label"
    self.value  =   p_value or nil     -- Value to be displayed after the label

    local width =   0
    if self.value then
        width   =   big_font:getWidth(string.gsub(_(p_label) .." : " .. _(p_value)," ","_"))
    else
        width   =   big_font:getWidth(string.gsub(_(p_label)," ","_"))
    end
    local height=   big_font:getHeight(string.gsub(_(p_label)," ","_"))

    self.rect   =   Rectangle(0, 0, width, height )

    self.hovered=   false
    self.selected=  false
    self.clicked=   false

end

function MenuItem:update(dt)
    self.hover          =   false
    --Control for value change: recalibrating hitbox to new size
    if _(self.value) ~= self.previousvalue or _(self.label) ~= self.previouslabel then
        local a, b  =   self.rect:getCenter()
        local width =   0
        if self.value then
            width   =   big_font:getWidth(string.gsub(_(self.label) .." : " .. _(self.value)," ","_"))
        else
            width   =   big_font:getWidth(string.gsub(_(self.label)," ","_"))
        end

        self.rect.width     =   width
        self.rect:setCenter(a, b)
        self.previousvalue  =   _(self.value)
        self.previouslabel  =   _(self.label)
    end

end

function MenuItem:setPos(p_x, p_y)
    self.rect:setPos(p_x, p_y)
end

function MenuItem:contains(p_x, p_y)
    return self.rect:contains(p_x, p_y)
end

function MenuItem:draw()
    if self.selected or self.hover then
        love.graphics.setColor(255, 255, 255, 255)
    else
        love.graphics.setColor(100, 155, 100, 255)
    end
    local text  =   _(self.label)
    if self.value then text = text .. " : " .. _(self.value) end

    love.graphics.printf(text, self.rect.x, self.rect.y, self.rect.width, "center")

    if DEBUG then self.rect:draw() end
end

function MenuItem:click()
    if self.clicksound then
        self.clicksound:stop()
        self.clicksound:play()
    end
end

function MenuItem:onHover()
    self.hover  =   true
end

function MenuItem:setCenter(p_x, p_y)
    self.rect:setCenter(p_x, p_y)
end
