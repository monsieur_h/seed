------------------------------------------------------------------------
-- @class ResourceBar
-- Class representing a ResourceBar
------------------------------------------------------------------------


class "ResourceBar"{
}

function ResourceBar:__init__()
    self.color  =   {r=255, g=255, b=255, a=255}
    self.res    =   nil
    self.hover  =   false
    self.preview=   0
    self.pos    =   {}
    self.scale  =   2
    
    --Utils for evolution display
    self.previousres    =   0
    self.time			=	0
    self.lastUpdate 	=	0
    self.updateEach		=	1 	--Update each... 1s?
    self.deltaRes		=	0	--Difference between res level 
    self.flashTime		=	1 	--Time to flash

end

function ResourceBar:listenTo(p_resource)
    self.res        	=   p_resource
    self.previousres	=   self.res.current

end

function ResourceBar:update(dt)
    self.time	=	self.time + dt
    
    --Shall we update the arrows?
    if (self.time - self.lastUpdate) > self.updateEach then
		self.deltaRes		=	self.res.current - self.previousres 
		self.lastUpdate		=	self.time
		self.previousres	=   self.res.current
		
    end
    self.hover  =   false
end

function ResourceBar:draw()

    --Drawing grey line
    love.graphics.setColor(230, 230, 230, 255)
    love.graphics.setLineWidth(3)
    love.graphics.line(self.pos.x, self.pos.y, self.pos.x, self.pos.y - self.res.max * self.scale)


    --Drawing current resource line
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, 255)
    love.graphics.setLineWidth(7)
    love.graphics.line(self.pos.x, self.pos.y, self.pos.x, self.pos.y - self.res.current * self.scale)


    -- Drawing the top (circle)
    if self.res.current == self.res.max then
        love.graphics.setColor(self.color.r, self.color.g, self.color.b, 185)
        love.graphics.circle("fill", self.pos.x,  self.pos.y - self.res.max * self.scale, 8)
        love.graphics.setColor(self.color.r, self.color.g, self.color.b, 255)
    else
        love.graphics.setColor(230, 230, 230, 255)
    end
    

    love.graphics.circle("fill", self.pos.x,  self.pos.y - self.res.max*self.scale, 5)
    
    
    --Drawing the flash (if resources are missing)
	if self.res then
		if (self.time - self.res.lastCantAffordTime) < self.flashTime then
			local ratio = 1-(self.time - self.res.lastCantAffordTime) / self.flashTime
			love.graphics.setLineStyle( "smooth" )
			love.graphics.setLineWidth( 8 )
			love.graphics.setColor( 255, 255, 255, 255 * ratio )
			love.graphics.line(self.pos.x, self.pos.y, self.pos.x, self.pos.y - self.res.max * self.scale)
			
			love.graphics.setLineWidth( 10 )
			love.graphics.setColor( 255, 255, 255, 150 * ratio )
			love.graphics.line(self.pos.x, self.pos.y, self.pos.x, self.pos.y - self.res.max * self.scale)
			
			love.graphics.setLineWidth( 12 )
			love.graphics.setColor( 255, 255, 255, 70 * ratio )
			love.graphics.line(self.pos.x, self.pos.y, self.pos.x, self.pos.y - self.res.max * self.scale)
			
		end
	end

    
    --Drawing the evolution arrows
    local cx, cy    =   self.pos.x, self.pos.y - self.res.max*self.scale
    local lo        =   6   --Line offset
    local na        =   math.abs( math.floor( self.deltaRes ) )
    if self.deltaRes < 0 then lo = -lo end --Make arrow look down if we're loosing resources
    if math.abs( na ) < 1 and math.abs( self.deltaRes * 100 ) / 100 ~= 0 then na=na+1 end

    love.graphics.setLineWidth( 3 )
    love.graphics.setColor( self.color.r, self.color.g, self.color.b, self.color.a )
    for i=1,(math.abs(na)) do
        love.graphics.line( cx - lo, cy - i*lo, cx, (cy - lo)-i*lo, cx + lo, cy - i*lo )
    end
    

    -- Drawing preview of a purchase
    if self.res.preview ~= 0 then
        love.graphics.setLineWidth( 3 )
        love.graphics.setColor( 255, 255, 255, 185 )
        love.graphics.line( self.pos.x,  self.pos.y - self.res.max*self.scale, self.pos.x,   self.pos.y - self.res.current * self.scale + self.res.preview * self.scale )

    end

    --Drawing tooltip
    if self.hover then
        --~ Drawing current value
        love.graphics.setFont(small_font)
        love.graphics.setColor(255, 255, 255, 255)
        local text      =   math.floor(self.res.current) .. "/" .. self.res.max 
        if self.name then text = _(self.name).." : "..text end
        local offset    =   small_font:getHeight(text)
        local y_pos     =   self.pos.y - (self.res.max * self.scale) - 20
        y_pos           =   y_pos - offset --aligning with middle of the text
        love.graphics.print(text, self.pos.x + 20, y_pos)
    end

end

function ResourceBar:onClick()
    return false
end

function ResourceBar:isAlive()
    return true
end

function ResourceBar:testPoint(p_x, p_y)
    local testx = false
    local testy = false
    if p_x > self.pos.x - 10 and p_x < self.pos.x + 10 then
        testx   =   true
    end

    if p_y < self.pos.y and p_y > self.pos.y - (self.res.max * self.scale) then
        testy   =   true
    end
    return (testx and testy)

end

function ResourceBar:onHover(p_x, p_y)
    self.hover  =   true
end

function ResourceBar:setPos(p_x, p_y)
    assert(p_x, "Needs a X pos")
    assert(p_y, "Needs a Y pos")
    self.pos.x, self.pos.y  =   p_x, p_y

end

function ResourceBar:setName(p_name)
    assert(p_name)
    self.name   =   p_name
end
