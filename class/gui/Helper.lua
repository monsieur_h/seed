------------------------------------------------------------------------
-- @class Helper
-- Displays infos at the bottom of the window
------------------------------------------------------------------------

class "Helper"{
}

function Helper:__init__(p_file)
    self.message    =   nil
    self.last       =   0
    self.age        =   0
    self.fadeTime   =   0.4
    self.displayTime=   5
    self.alpha      =   255
    self.margin     =   10 --space from edges of screen
    self.pos        =   {x=0, y=0, max_y= self.margin, offset= 0}
    self.pos.x      =   0 + self.margin
    self.pos.y      =   love.graphics.getHeight() - self.margin
    self.file       =   p_file or "hints"
end

function Helper:setFile(p_file)
    assert(p_file)
    self.file   =   p_file
    if love.filesystem.exists(p_file) then
        local fullpath = love.filesystem.getSaveDirectory().."/"..p_file
        print("Loading hints from "..fullpath)
        self.data      =   table.load(fullpath)
        print("Hints file found and loaded!")
        if self.data["version"] ~= PROJECT_VERSION then
            print("The hint file was made with a different version: LET'S ERASE!")
            self:setDefaults()
            self:saveToFile()
        end

    else
        print("No hint file found, creating w/ defaults...")
        self:setDefaults()
        self:saveToFile()
    end
    self.data.random    =   {}
    for i, v in pairs(self.data) do
        if v.random then
            self.data.random[i]=v
            print(self.data.random[i])
        end
    end
end

function Helper:setDefaults()
    local dir       =   love.filesystem.getSaveDirectory()
        self.data   =   love.filesystem.load( "data/hints/hint.lua" )()
        print("Default hints data loaded")
end

function Helper:saveToFile()
    if self.file then
        local dir       =   love.filesystem.getSaveDirectory()
        if not love.filesystem.isDirectory(dir) then
            love.filesystem.mkdir(dir)
        end
        local fullpath  =   dir.."/"..self.file
        print("Saving hints to "..fullpath)
        table.save(self.data, fullpath)
    end
end

function Helper:update(dt)
    self.age    =   self.age + dt

    if self.message then
        local delta =   self.age - self.last
        if delta < self.fadeTime then --fading in
            self.alpha  =   delta / self.fadeTime
            self.alpha  =   self.alpha * 255
            self.pos.y  =   1 - (delta / self.fadeTime)
            self.pos.y  =   self.pos.max_y + self.pos.y * self.pos.offset

        elseif delta < (self.fadeTime+self.displayTime) then --displaying
            self.alpha  =   255
            self.pos.y  =   self.pos.max_y

        elseif delta < (self.fadeTime*2+self.displayTime) then --fading out
            self.alpha  =   delta - (self.fadeTime + self.displayTime)
            self.alpha  =   self.alpha / self.fadeTime
            self.alpha  =   255 - ( self.alpha * 255 )
            self.pos.y  =   delta - (self.fadeTime + self.displayTime)
            self.pos.y  =   self.pos.y / self.fadeTime
            self.pos.y  =   self.pos.max_y + self.pos.y * self.pos.offset
        else -- Message is over
            self.message = nil
            self.pos.y  =   self.pos.max_y + self.pos.offset

        end

    end

    if self.randomInterval then
        if (self.age - self.lastRandom) > self.randomInterval then
            self:sendRandomHint()
        end
    end
end

function Helper:draw()
    if self.message then
        love.graphics.setFont( self.font )
        love.graphics.setColor( 255, 255, 255, self.alpha )
        love.graphics.print( self.message, self.pos.x, self.pos.y)
    end
end

function Helper:setFont(p_font)
    assert(p_font)
    print("font set")
    self.font   =   p_font
    self.pos.y  =   love.graphics.getHeight() - self.margin - self.font:getHeight()
    self.pos.max_y  =   self.pos.y
    self.pos.offset =   self.margin
end

function Helper:send(p_message)
    assert(p_message)
    self.message=   _(p_message)
    self.last   =   self.age

end

function Helper:safeSend(p_message)
    if self:canSend(p_message) then
        self:send(p_message)
    end
end

function Helper:canSend(p_message)
    if self.data then
        if self.data[p_message] then
            if (self.data[p_message].current < self.data[p_message].max)then
                self.data[p_message].current = self.data[p_message].current + 1
                return true

            elseif self.data[p_message].max==0 then
                print("max is 0 -> infiite spam")
                return true
                --~ self:send(p_message)

            end
        else
            return false

        end
    else
        return false
    end
end

function Helper:sendRandomHint()
    self.lastRandom =   self.age
    if self.data  and self.data.random then

        for text, hint in pairs(self.data) do
            if hint.random == true and ( hint.current < hint.max )then
                hint.current    =   hint.current + 1
                self:send(text)
                return true
            end
        end
        return false
    else
        return false
    end

end

function Helper:getTimeSinceLastMessage()
    return self.age - self.last
end

function Helper:setInterval(p_time)
    assert(p_time)
    self.lastRandom     =   self.age
    self.randomInterval =   p_time
end
