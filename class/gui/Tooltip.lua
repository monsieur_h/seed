------------------------------------------------------------------------
-- @class Tooltip
-- Class displaying a tooltip for any element
------------------------------------------------------------------------


class "Tooltip"{
}

-- Pos is reprensenting the midleft position of the rectangle of the tooltip
function Tooltip:__init__( p_element, p_pos, p_desc )
	assert( p_element, "Target element needed" )
	print( "Creating a tooltip" )
	self.target	=	p_element
	self.time	=	0
	self.pos	=	p_pos or vector( love.graphics.getWidth(), love.graphics.getHeight()/2 )
	self.desc	=	p_desc or ""
	self.dead	=	false
	self.padding	=	25
	self.margin	=	15
	self.rect	=	Rectangle( self.pos.x + self.margin, self.pos.y, 350, 200 )
end

function Tooltip:update( dt )
	self.time	=	self.time + dt
	local x,y 	=	seedcam:cameraCoords( self.pos.x, self.pos.y )
	self.rect:setMidLeft( x + self.margin * 2, y )
end

function Tooltip:draw()
	love.graphics.setFont( small_font )
	
	--Drawing border
	love.graphics.setLine( 3 )
	love.graphics.setColor( 0, 0, 0, 255 )
	self.rect:draw()
	
	--Drawing arrow
	love.graphics.setLine( self.margin / 3 )
	local midleftx, midlefty	= self.rect:getMidLeft()
	love.graphics.line( midleftx, midlefty - self.margin, midleftx - self.margin, midlefty, midleftx, midlefty + self.margin )
	
	--Drawing background of tooltip
	love.graphics.setColor( 120, 120, 120, 185 )
	love.graphics.rectangle( "fill", self.rect.x, self.rect.y, self.rect.width, self.rect.height )
	
	--Drawing text
	love.graphics.setColor( 255, 255, 255, 255 )
	love.graphics.printf( self.desc, self.rect.x + self.padding, self.rect.y + self.padding, self.rect.width - self.padding, "left" )
end

function Tooltip:isAlive()
	return not self.dead
end

function Tooltip:testPoint( p_x, p_y )
	return self.rect:testPoint( p_x, p_y )
end

function Tooltip:onHover()
	
end

function Tooltip:onClick( m_x, m_y )
	if self:testPoint( m_x, m_y ) then -- If clicked on me : die
		self.dead = true
	end
end
