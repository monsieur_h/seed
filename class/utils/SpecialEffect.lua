------------------------------------------------------------------------
-- @class SpecialEffect
-- Loads and apply special effects
------------------------------------------------------------------------

class "SpecialEffect"{
}

function SpecialEffect:__init__()
    self.effects    =   {}
    self.active     =   false

end

function SpecialEffect:toggle()
    self.active =   not self.active

end

function SpecialEffect:loadShader(p_name)
    assert(p_name, "SpecialEffect:loadShaderByName needs a name as parameter")
    local shader, error    =   dofile("data/shaders/" .. p_name..".lua")
    assert(not error, "Error while loading shader")

    self.effects[p_name]   =    love.graphics.newPixelEffect(shader)
end

function SpecialEffect:getEffect(p_name)
    assert(p_name, "SpecialEffect:getEffect : no effect name")
    assert(self.effects[p_name], "Effect "..p_name.." not loaded")
    return self.effects[p_name]

end

function SpecialEffect:apply(p_name)
    if self.active then
        assert(p_name, "SpecialEffect:apply : no effect name")
        assert(self.effects[p_name], "Effect "..p_name.." not loaded")
        love.graphics.setPixelEffect(self.effects[p_name])
    end
end

function SpecialEffect:stop()
    love.graphics.setPixelEffect()
end

function SpecialEffect:sendMousePos(p_x, p_y, p_name)
    assert(p_x, "Needs a X coord")
    assert(p_y, "Needs a Y coord")
    assert(p_name, "no effect name")
    assert(self.effects[p_name], "Effect "..p_name.." not loaded")
    local mousepos = {p_x, p_y}
    self.effects[p_name]:send("mousepos", mousepos)

end
