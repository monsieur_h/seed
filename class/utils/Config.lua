------------------------------------------------------------------------
-- @class Config
-- Used to read/write configuration from the config file
-- REQUIRE table.save
------------------------------------------------------------------------

class "Config"{
}

-- Constructor
function Config:__init__(p_filename)
    print("New config object")
    if not p_filename then
        p_filename  =   "config"
    end
    self.options    =   {}
    self:setFile(p_filename)
end

-- Sets and loads a file if exists. If not, sets to default and save it
function Config:setFile(p_filename)
    self.file   =   p_filename
    if love.filesystem.exists(p_filename) then
        local fullpath = love.filesystem.getSaveDirectory().."/"..p_filename
        print("Loading "..fullpath.."  "..type(fullpath))
        self.options    =   table.load(fullpath)
        print("Config file found and loaded!")
        if self.options["version"] ~= PROJECT_VERSION then
            print("The config file was made with a different version: LET'S ERASE!")
            self:setDefaults()
            self:saveToFile()
        end
    else
        print("No config file found, creating w/ defaults...")
        self:setDefaults()
        self:saveToFile()
    end
end

function Config:setDefaults()
    self.options                =   {}
    self.options["width"]       =   1024
    self.options["height"]      =   768
    self.options["fullscreen"]  =   "off"
    self.options["music"]       =   "on"
    self.options["sound"]       =   "on"
    self.options["music_vol"]   =   1.0
    self.options["sound_vol"]   =   1.0
    self.options["locale"]      =   "en"
    self.options["scroll_speed"]=   10
    self.options["post-process"]=   "off"
    self.options["version"]     =   PROJECT_VERSION or "unknown"
end

function Config:saveToFile(p_filename)
    if not p_filename then
        p_filename=self.file
    end
    local dir       =   love.filesystem.getSaveDirectory()
    if not love.filesystem.isDirectory(dir) then
        love.filesystem.mkdir(dir)
    end
    local fullpath  =   dir.."/"..self.file
    print("Saving to "..fullpath)
    table.save(self.options, fullpath)
end

function Config:toggle(p_value)
    assert(p_value, "Trying to toggle unknown option")
    if not self.options[p_value] then
        self.options[p_value]   =   "on"
    elseif self.options[p_value]=="on" then
        self.options[p_value]   =   "off"
    else
        self.options[p_value]   =   "on"
    end

end
