------------------------------------------------------------------------
-- @class Triangle
-- Class representing a triangle
------------------------------------------------------------------------

class "Triangle"{
}

function Triangle:__init__(...)
    self.a  =   {}
    self.b  =   {}
    self.c  =   {}
    if #arg == 6 then
        self.a.x    =   arg[1]
        self.a.y    =   arg[2]
        self.b.x    =   arg[3]
        self.b.y    =   arg[4]
        self.c.x    =   arg[5]
        self.c.y    =   arg[6]

    elseif #arg ==  3 then
        self.a.x, self.a.y  =   arg[1].x, arg[1].y
        self.b.x, self.b.y  =   arg[2].x, arg[2].y
        self.c.x, self.c.y  =   arg[3].x, arg[3].y

    end

end

function Triangle:getArea()
    -- 2 vects
    -- dot product
    -- Norm of result
    -- /2
    --~ local cross_product =   {}
    cross_product.x     =   self.a.x * self.b.x
    cross_product.y     =   self.a.y * self.b.y

    --~ local dot_product   =   self.a.x * self.b.x + self.a.y * self.b.y
    local area          =   math.sqrt(cross_product.x^2, cross_product.y^2) / 2

end
