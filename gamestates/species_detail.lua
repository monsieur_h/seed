------------------------------------------------------------------------
-- SPECIES DETAILS
------------------------------------------------------------------------
speciesdetail                 =   Gamestate.new()
speciesdetail.position        =   vector(0, 0) --redifined when we click on seed's icon from newgame menu
speciesdetail.rotation        =   0
speciesdetail.zoom            =   1
speciesdetail.items           =   {}
speciesdetail.alpha           =   0 --Alpha du menu pour transition

function speciesdetail:init() -- runs only once
    print("Entering SPECIES DETAIL state")
    self.validated      =   false
    playitem  =   ImageButton()
    playitem:loadImage("res/images/gui/main_menu/branch_top_hover.png")
    playitem:loadIdleImage("res/images/gui/main_menu/branch_top_idle.png")
    playitem:setCenter(100, 200)
    playitem:setClickableZone(400, 200)
    playitem:loadHoverSound("res/sounds/button_onHover.wav")
    function playitem:onClick()
        print("Chosen species: "..current_species.name)
        speciesdetail.validated =   true
        seedicon.rotation   =   - seedicon.angle / TRANSITION.TIME
        print("seedicon.rotation   =   - seedicon.angle / TRANSITION.TIME")
        print(seedicon.rotation..  " =   - "..seedicon.angle.. "/".. TRANSITION.TIME)
        if not TRANSITION.ONGOING then speciesdetail.alpha = 255 end
        seeding.species =   current_species
        TRANSITION.TIME =   2.5
        seeding.position    =   speciesdetail.position
        TRANSITION:goTo(seeding)
    end

    seedicon            =   {}
    seedicon.image      =   current_species.preview_icon
    seedicon.seed       =   current_species.seed_image
    seedicon.angle      =   0
    seedicon.pos        =   {}
    seedicon.pos.x      =   love.graphics.getWidth()/2
    seedicon.pos.y      =   love.graphics.getHeight()/2
    seedicon.offset     =   {}
    seedicon.offset.x   =  seedicon.image:getWidth()/2
    seedicon.offset.y   =  seedicon.image:getHeight()/2
    seedicon.alpha      =  255
    seedicon.rotation   =  1

    function seedicon:draw()
        love.graphics.setColor(255, 255, 255, self.alpha)
        love.graphics.draw(self.image, self.pos.x, self.pos.y, self.angle, 1, 1, self.offset.x, self.offset.y)
        love.graphics.setColor(255, 255, 255, 255 - self.alpha)
        love.graphics.draw(self.seed, self.pos.x, self.pos.y, self.angle, 1, 1, self.offset.x, self.offset.y)
    end

    function seedicon:update(dt)
        if TRANSITION.ONGOING then self.alpha = self.alpha - dt / TRANSITION.TIME * 255 end
        if speciesdetail.validated and self.angle <= math.pi/10 then
            --Plain nothing, just to see
            self.angle  =   0
        else
            self.angle   =   self.angle + self.rotation * dt
            if self.angle > 2*math.pi then
                self.angle  =   self.angle - 2 * math.pi
            end
        end
    end

end

function speciesdetail:update(dt)
    hint:update(dt)
    if TRANSITION.ONGOING then
        self.alpha = self.alpha - dt / TRANSITION.TIME * 255

    elseif self.alpha ~= 255 then
        self.alpha  =   self.alpha + 200 * dt
        self.alpha  =   clamp(self.alpha, 0, 255)

    end
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)

    TRANSITION:update(dt)

    seedicon:update(dt)

end

function speciesdetail:enter(previous) -- run every time the state is entered
    preview_alpha   =   0
    angle           =   0
    TRANSITION.TIME =   0.5 -- Fast transition to the level.
    --~ menucam.rot         =   math.rad(speciesdetail.rotation)
    hint:safeSend("The play button is off duty press enter to confirm.")

end

function speciesdetail:draw()
    if self.validated then
        love.graphics.setColor(self.alpha, self.alpha, self.alpha, 255)
    else
        love.graphics.setColor(255, 255, 255, 255)
    end
    menucam:attach()

    --Drawing splash image
    love.graphics.draw(background.image, background.pos:unpack())

    menucam:detach()

    playitem:draw( self.alpha )

    love.graphics.setColor(255, 255, 255, self.alpha)
    if current_species.preview_image then

        local w =   current_species.preview_image:getWidth()
        local h =   current_species.preview_image:getHeight()
        love.graphics.draw(current_species.preview_image, love.graphics.getWidth() - w)
    end


    -- Draw the image of the seed
    seedicon:draw()

    -- Draw the info text
    if self.validated then
        love.graphics.setColor(self.alpha, self.alpha, self.alpha, 255)
    else
        love.graphics.setColor(255, 255, 255, self.alpha)
    end
    love.graphics.setFont(title_font)
    love.graphics.printf(_(current_species.name), 0, 80, love.graphics.getWidth(), "center")
    love.graphics.setFont(standard_font)
    desc_string =   _("Description")..":\t".._(current_species.description).."\n"
    .._("Jumps")..":\t"..current_species.max_jumps.."\n"
    .."Max branches:\t"..current_species.max_branches .."\n"
    .."Max roots:\t"..current_species.max_roots
    love.graphics.printf(_(desc_string), love.graphics.getWidth()/8, 540, love.graphics.getWidth()/2, "left")


    love.graphics.setColor(255, 255, 255, 255)
    watermark()
    hint:draw()
    if DEBUG then debug_draw_stats(menucam) end

end

function speciesdetail:keyreleased(key)

    if key == "escape" then
        TRANSITION:goTo(newgame)
    end

    if key == "return" and not speciesdetail.validated then
        print("Chosen species: "..current_species.name)
        speciesdetail.validated =   true
        seedicon.rotation   =   - seedicon.angle / TRANSITION.TIME
        print("seedicon.rotation   =   - seedicon.angle / TRANSITION.TIME")
        print(seedicon.rotation..  " =   - "..seedicon.angle.. "/".. TRANSITION.TIME)
        if not TRANSITION.ONGOING then speciesdetail.alpha = 255 end
        seeding.species =   current_species
        TRANSITION.TIME =   2.5
        seeding.position    =   speciesdetail.position
        TRANSITION:goTo(seeding)
    end
end

function speciesdetail:mousereleased(m_x, m_y, button)
    local x, y = menucam:mousepos()
    if button == "l" then
        if playitem:contains(m_x, m_y) then
            playitem:onClick()
        end
    elseif button == "r" then
        speciesdetail.alpha = 255
        TRANSITION:goTo( newgame )
    end

end
