------------------------------------------------------------------------
-- OPTIONS MENU - allows to change sound and display parameters
------------------------------------------------------------------------
optionmenu              =   Gamestate.new()
optionmenu.position     =   vector(1050, 140)
optionmenu.rotation     =   0
optionmenu.zoom         =   1
optionmenu.alpha        =   0
optionmenu.items        =   {}

function optionmenu:init()
    graphics    =   ImageButton()
    graphics:loadIdleImage("res/images/gui/main_menu/options/Graphic/graphics_idle.png")
    graphics:loadLabelImage("res/images/gui/main_menu/options/Graphic/graphics_hover.png")
    graphics:loadSelectedImage("res/images/gui/main_menu/options/Graphic/graphics_active.png")
    graphics:setCenter(1100, 160)
    function graphics:onClick()
	graphics.alpha  =   255
        self.selected   =   true
        self.glow       =   255
        graphicsoption.back =   self
        TRANSITION:goTo( graphicsoption )
    end
    table.insert(self.items, graphics)

    language    =   ImageButton()
    language:loadIdleImage("res/images/gui/main_menu/options/Languages/Language.png")
    language:loadLabelImage("res/images/gui/main_menu/options/Languages/Language_glow.png")
    language:setCenter(1050, 270)
    language.value  =   GAMECONFIG.options["locale"]
    function language:onClick()
	graphics.alpha  =   255
        self.selected   =   false --Prevent selection because no sub-menu
        if GAMECONFIG.options["locale"]=="en" then
            GAMECONFIG.options["locale"]="fr"
        else
            GAMECONFIG.options["locale"]="en"
        end
        self.value  =   GAMECONFIG.options["locale"]
        setLocale(GAMECONFIG.options["locale"])
        GAMECONFIG:saveToFile()
    end
    table.insert(self.items, language)

    sound       =   ImageButton()
    sound:loadIdleImage("res/images/gui/main_menu/options/Sound/sound_idle.png")
    sound:loadLabelImage("res/images/gui/main_menu/options/Sound/sound_hover.png")
    sound:loadSelectedImage("res/images/gui/main_menu/options/Sound/sound_active.png")
    sound:setCenter(1050, 40)
    function sound:onClick()
        self.selected   =   true
        self.glow       =   255
        soundoption.back=   self
        TRANSITION:goTo( soundoption )
    end
    table.insert(self.items, sound)
end

function optionmenu:enter()
    self.alpha  =   0
    for i, v in pairs(self.items) do
        v.selected  =   false
    end
    hint:safeSend( "Right-click to move to the previous menu" )
end

function optionmenu:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)

    --Alpha for transition
    if TRANSITION.ONGOING then
        self.alpha = self.alpha - dt / TRANSITION.TIME * 255

    elseif self.alpha ~= 255 then
        self.alpha  =   self.alpha + 1000 * dt
        self.alpha  =   clamp(self.alpha, 0, 255)
    --~ elseif self.alpha >= 255 then --resetting alpha for previously selected element
        --~ for i, v in pairs(self.items) do
            --~ v.selected  =   false
        --~ end

    end

    --Checking hovers & updating
    m_x, m_y = menucam:mousepos()
    for i, button in pairs(self.items) do
        if button.rect:inflate(-150, -400):contains(m_x, m_y) and not TRANSITION.ONGOING then
            button.hover    =   true
        else
            button.hover    =   false
        end
        button:update(dt)
    end
    hint:update(dt)
end

function optionmenu:draw()
    menucam:attach()
    --Drawing back image
    love.graphics.draw(background.image, background.pos:unpack())


    for i, item in pairs(self.items) do
        if not item.selected then
            item:draw(optionmenu.alpha)
        else
            item:draw()
        end
    end
    menucam:detach()
    if DEBUG then debug_draw_stats(menucam) end
    hint:draw()
    watermark()
end

function optionmenu:keyreleased(key, unicode)
    if key== " " then tprint(language) end
    if key=="escape" then
    	optionmenu.alpha = 255
        TRANSITION:goTo(mainmenu)
    end
end

function optionmenu:mousereleased(m_x, m_y, button)
    if button == "l" then
        m_x, m_y = menucam:mousepos()
        for i, button in pairs(self.items) do
            if button.rect:inflate(-150, -380):contains(m_x, m_y) then
                button.selected    =   true
                button:onClick()
            else
                button.selected    =   false
            end
        end

    elseif button == "r" then
    	optionmenu.alpha = 255
        TRANSITION:goTo(mainmenu)
    end
end
