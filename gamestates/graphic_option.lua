------------------------------------------------------------------------
-- GRAPHICS_OPTION - let's choose thoses graphical options
------------------------------------------------------------------------

graphicsoption              =   Gamestate.new()
graphicsoption.position     =   vector(1150, 140)
graphicsoption.rotation     =   0
graphicsoption.zoom         =   1.15
graphicsoption.items        =   {}
graphicsoption.alpha        =   0

function graphicsoption:enter()
    self.alpha  =   0

end

function graphicsoption:init()
    print("Entering display options")
    if GAMECONFIG.options["fullscreen"]     then fs = "on" else fs = "off" end
    if GAMECONFIG.options["post-process"]   then pp = "on" else pp = "off" end


    resolution          =   ImageButton()
    resolution:loadIdleImage("res/images/gui/main_menu/options/Graphic/Submenu/resolution.png")
    resolution:loadLabelImage("res/images/gui/main_menu/options/Graphic/Submenu/resolution_glow.png")
    resolution:setCenter(1060, 40)
    local w, h          =   love.graphics.getMode()
    resolution.value    =   w.."x"..h
    function resolution:onClick()

    end
    table.insert(self.items, resolution)

    fullscreen          =   ImageButton()
    fullscreen:loadIdleImage("res/images/gui/main_menu/options/Graphic/Submenu/fullscreen.png")
    fullscreen:loadLabelImage("res/images/gui/main_menu/options/Graphic/Submenu/fullscreen_glow.png")
    fullscreen:setCenter(1130, 160)
    fullscreen.value    =   fs
    function fullscreen:onClick()
        GAMECONFIG:toggle("fullscreen")
        self.value  =   GAMECONFIG.options["fullscreen"]
        GAMECONFIG:saveToFile()

    end
    table.insert(self.items, fullscreen)

    postprocess          =   ImageButton()
    postprocess:loadIdleImage("res/images/gui/main_menu/options/Graphic/Submenu/post_process.png")
    postprocess:loadLabelImage("res/images/gui/main_menu/options/Graphic/Submenu/post_process_glow.png")
    postprocess:setCenter(1120, 270)
    postprocess.value    =   pp
    function postprocess:onClick()
        GAMECONFIG:toggle("post-process")
        self.value  =   GAMECONFIG.options["post-process"]
        GAMECONFIG:saveToFile()
    end
    table.insert(self.items, postprocess)

end

function graphicsoption:update(dt)
    self.back:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)

    --Alpha for transition
    if TRANSITION.ONGOING then
        self.alpha = self.alpha - dt / TRANSITION.TIME * 255

    elseif self.alpha ~= 255 then
        self.alpha  =   self.alpha + 1000 * dt
        self.alpha  =   clamp(self.alpha, 0, 255)

    end

    --Checking hovers & updating
    m_x, m_y = menucam:mousepos()
    for i, button in pairs(self.items) do
        if button.rect:inflate(-150, -400):contains(m_x, m_y) and not TRANSITION.ONGOING then
            button.hover    =   true
        else
            button.hover    =   false
        end
        button:update(dt)
    end
end

function graphicsoption:keyreleased(key, unicode)
    if key == "esc" then
        TRANSITION:goTo( optionmenu )
    end
end

function graphicsoption:mousereleased(m_x, m_y, button)
    if button == "l" then
        m_x, m_y = menucam:mousepos()
        for i, button in pairs(self.items) do
            if button.rect:inflate(-150, -380):contains(m_x, m_y) then
                button:onClick()
            end
        end

    elseif button == "r" then
    	graphicsoption.alpha = 255
        TRANSITION:goTo( optionmenu )
    end
end

function graphicsoption:draw()
    menucam:attach()
    --Drawing back image
    love.graphics.draw(background.image, background.pos:unpack())
    self.back:draw()

    love.graphics.setColor(255, 255, 255, graphicsoption.alpha)

    for i, item in pairs(self.items) do
        item:draw(graphicsoption.alpha)
    end
    menucam:detach()
    if DEBUG then debug_draw_stats(menucam) end
    watermark()

end
