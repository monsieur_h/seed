------------------------------------------------------------------------
-- SOUND_OPTION - let's choose thoses graphical options
------------------------------------------------------------------------

soundoption             =   Gamestate.new()
soundoption.position    =   vector(1050, 40)
soundoption.rotation    =   0
soundoption.zoom        =   1.15
soundoption.items       =   {}
soundoption.alpha       =   0

function soundoption:init()
    print("Entering sound options!")
    musicbutton     =   ImageButton()
    musicbutton:loadIdleImage("res/images/gui/main_menu/options/Sound/Submenu/Music.png")
    musicbutton:loadLabelImage("res/images/gui/main_menu/options/Sound/Submenu/Music_glow.png")
    musicbutton:setCenter(1070, 25)
    musicbutton.value =   GAMECONFIG.options["music"]
    function musicbutton:onClick()
        GAMECONFIG:toggle("music")
        self.value  =   GAMECONFIG.options["music"]
        GAMECONFIG:saveToFile()
    end
    table.insert(soundoption.items, musicbutton)

    soundbutton     =   ImageButton()
    soundbutton:loadIdleImage("res/images/gui/main_menu/options/Sound/Submenu/Sound.png")
    soundbutton:loadLabelImage("res/images/gui/main_menu/options/Sound/Submenu/Sound_glow.png")
    soundbutton:setCenter(1020, 120)
    soundbutton.value = GAMECONFIG.options["sound"]
    function soundbutton:onClick()
        GAMECONFIG:toggle("sound")
        self.value  =   GAMECONFIG.options["sound"]
        GAMECONFIG:saveToFile()
    end
    table.insert(soundoption.items, soundbutton)

    volumebutton    =   ImageButton()
    volumebutton:loadIdleImage("res/images/gui/main_menu/options/Sound/Submenu/Volume.png")
    volumebutton:loadLabelImage("res/images/gui/main_menu/options/Sound/Submenu/Volume_glow.png")
    volumebutton:setCenter(1010, 215)
    volumebutton.value  =   GAMECONFIG.options["music_vol"]
    function volumebutton:onClick()
        local vol = GAMECONFIG.options["music_vol"] +0.1
        if vol > 1.0 then vol = 0 end
        GAMECONFIG.options["music_vol"] = vol
        self.value  =   vol
        GAMECONFIG:saveToFile()
    end
    table.insert(soundoption.items, volumebutton)
end

function soundoption:enter()
    self.alpha  =   0
end

function soundoption:update(dt)
    self.back:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)

    --Alpha for transition
    if TRANSITION.ONGOING then
        self.alpha = self.alpha - dt / TRANSITION.TIME * 255

    elseif self.alpha ~= 255 then
        self.alpha  =   self.alpha + 1000 * dt
        self.alpha  =   clamp(self.alpha, 0, 255)

    end

    --Checking hovers & updating
    m_x, m_y = menucam:mousepos()
    for i, button in pairs(self.items) do
        if button.rect:inflate(-150, -400):contains(m_x, m_y) and not TRANSITION.ONGOING then
            button.hover    =   true
        else
            button.hover    =   false
        end
        button:update(dt)
    end
end

function soundoption:keyreleased(key, unicode)
    if key=="esc" then
        TRANSITION:goTo(optionmenu)
    end
end

function soundoption:mousereleased(m_x, m_y, button)
    if button == "l" then
        m_x, m_y = menucam:mousepos()
        for i, button in pairs(self.items) do
            if button.rect:inflate(-150, -380):contains(m_x, m_y) then
                button:onClick()
            end
        end

    elseif button == "r" then
	soundoption.alpha = 255
        TRANSITION:goTo( optionmenu )
    end
end

function soundoption:draw()
    menucam:attach()
    --Drawing back image
    love.graphics.draw(background.image, background.pos:unpack())
    self.back:draw()

    love.graphics.setColor(255, 255, 255, graphicsoption.alpha)

    for i, item in pairs(self.items) do
        item:draw(soundoption.alpha)
    end
    menucam:detach()
    if DEBUG then debug_draw_stats(menucam) end
    watermark()

end
