------------------------------------------------------------------------
-- MAINMENU - allows the player to start or load a game and such
------------------------------------------------------------------------

mainmenu                =   Gamestate.new()
mainmenu.position       =   vector(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
mainmenu.rotation       =   0
mainmenu.zoom           =   0.4
mainmenu.items          =   {}
mainmenu.alpha          =   0



-- Adding branch buttons
optionitem  =   ImageButton()
optionitem:loadImage("res/images/gui/main_menu/branch_right_hover.png")
optionitem:loadIdleImage("res/images/gui/main_menu/branch_right_idle.png")
optionitem:setCenter(1005, 122)
optionitem:setClickableZone(400, 400)
optionitem:loadHoverSound("res/sounds/button_onHover.wav")
optionitem:loadClickSound("res/sounds/button_onClick.wav")

function optionitem:onClick()
    mainmenu.alpha = 255
    self:click()
    TRANSITION:goTo(optionmenu)
end
table.insert(mainmenu.items, optionitem)

continueitem  =   ImageButton()
continueitem:loadImage("res/images/gui/main_menu/branch_left_hover.png")
continueitem:loadIdleImage("res/images/gui/main_menu/branch_left_idle.png")
continueitem:setCenter(-7, 187)
continueitem:setClickableZone(400, 400)
continueitem:loadHoverSound("res/sounds/button_onHover.wav")
continueitem:loadClickSound("res/sounds/button_onClick.wav")
function continueitem:onClick()
    mainmenu.alpha = 255
    self:click()
    print("Not yet")
end
table.insert(mainmenu.items, continueitem)

playitem  =   ImageButton()
playitem:loadImage("res/images/gui/main_menu/branch_top_hover.png")
playitem:loadIdleImage("res/images/gui/main_menu/branch_top_idle.png")
playitem:setCenter(463, -97)
playitem:setClickableZone(400, 400)
playitem:loadHoverSound("res/sounds/button_onHover.wav")
playitem:loadClickSound("res/sounds/button_onClick.wav")

function playitem:onClick()
    mainmenu.alpha = 255
    self:click()
    TRANSITION:goTo(newgame)
end
table.insert(mainmenu.items, playitem)

exititem  =   ImageButton()
exititem:loadImage("res/images/gui/main_menu/branch_bottom.png")
--~ exititem:loadIdleImage("res/images/gui/main_menu/label_quit.png")
exititem:setCenter(504, 750)
exititem:setClickableZone(1500, 600)
exititem:loadHoverSound("res/sounds/button_onHover.wav")
exititem:loadClickSound("res/sounds/button_onClick.wav")
function exititem:onClick()
    mainmenu.alpha = 255
    self:click()
    love.event.quit()
end
table.insert(mainmenu.items, exititem)


mainmenu.hud  =   {}

statsbutton   =   MenuItem("Stats")
statsbutton:setPos(love.graphics.getWidth() - statsbutton.rect.width, 10 )
function statsbutton:onClick()
    mainmenu.alpha = 255
    print("Stat clicked")
    TRANSITION:goTo(statmenu)
end

table.insert(mainmenu.hud, statsbutton)

function mainmenu:init()

end

function mainmenu:enter(previous)
    print("Entering main menu")
    TRANSITION.TIME =   0.5
    self.alpha      =   0
end

function mainmenu:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)
    --Alpha for transition
    if TRANSITION.ONGOING then
        self.alpha = self.alpha - dt / TRANSITION.TIME * 255

    elseif self.alpha ~= 255 then
        self.alpha  =   self.alpha + 100 * dt
        self.alpha  =   clamp(self.alpha, 0, 255)

    end

    m_x, m_y = menucam:mousepos()
    for i, button in pairs(mainmenu.items) do
        if button:contains(m_x, m_y) then
            button.hover    =   true
        else
            button.hover    =   false
        end
        button:update(dt)
    end

    x, y    =   love.mouse.getPosition()
    for i, hudelt in pairs(mainmenu.hud) do
        hudelt:update(dt)
        if hudelt:contains(x, y) then
            hudelt:onHover()
        end
    end
end

function mainmenu:draw()
    menucam:attach()
    if background then
        love.graphics.draw(background.image, background.pos:unpack())
    end
    for i, button in pairs(mainmenu.items) do
        button:draw( mainmenu.alpha )
    end

    menucam:detach()

    for i, hudelt in pairs(mainmenu.hud) do
        hudelt:draw( mainmenu.alpha )
    end
    if DEBUG then
        debug_draw_stats(menucam)
    end
    hint:draw()
    watermark()
end

function mainmenu:keyreleased(key, unicode)
    if key=="return" then
        Gamestate.switch(newgame)
    end
    if key == " "then
        hint:sendRandomHint()
    end
end

function mainmenu:mousereleased(m_x, m_y, button)
    local x, y = menucam:mousepos()
    for i, button in pairs(mainmenu.items) do
        if button:contains(x, y) then
            button:onClick()
        end
    end

    for i, hudelt in pairs(mainmenu.hud) do
        if hudelt:contains(m_x, m_y) then
            hudelt:onClick()
        end
    end
end
