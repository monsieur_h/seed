------------------------------------------------------------------------
-- NEW GAME
------------------------------------------------------------------------
newgame                 =   Gamestate.new()
newgame.position        =   vector(463, -97)
newgame.rotation        =   0
newgame.zoom            =   0.9
newgame.items           =   {} --list of menu item to be displayed

function newgame:init() -- run only once
    print("Entering NEW GAME state")
    print("Loading seed images")
    current_species =   nil

    specieschoice   =   ImageButton()
    specieschoice:loadIdleImage("res/images/gui/main_menu/play/Species_choice.png")
    specieschoice:loadSelectedImage("res/images/gui/main_menu/play/Species_choice_glow.png")
    specieschoice:loadHoverSound("res/sounds/button_onHover.wav")
    specieschoice:loadClickSound("res/sounds/button_onClick.wav")
    specieschoice:setCenter(463, -97)
    specieschoice.species_icons   =   {}
    function specieschoice:onClick()
	newgame.alpha = 255
        self:click()
        print("clicked item")
        self.selected   =   not self.selected
        for i,icon in pairs(self.species_icons) do

            icon.selected   =   self.selected -- If selected: they're displayed
            --~ icon.alpha = 255
            print("Passing icon to:")
            print( icon.hover )
        end
    end
    table.insert(newgame.items, specieschoice)

    -- Insert species here
    species_list        =   load_species()

    local angle_offset  =   (math.pi * 2) / #species_list
    local radius        =   #species_list * 10 + 100

    for i, sp in pairs(species_list) do
        local angle =   angle_offset * i
        local x =   (radius * math.cos(angle)) + newgame.position.x
        local y =   (radius * math.sin(angle)) + newgame.position.y
        local item  =   ImageButton()
        item:setImage(sp.preview_icon)
        item:setSelectedImage(sp.preview_icon)
        item:setCenter(x, y)
        item.species    =   sp
        function item:onClick()
	    newgame.alpha = 255
            if self.selected then
                self.glow  =   255
                --~ self.glw    =   0
                current_species =   self.species
                speciesdetail.position  =   vector(self.rect.center_x, self.rect.center_y)
                TRANSITION:goTo(speciesdetail)
                return true
            else
                return false
            end
        end
        table.insert(specieschoice.species_icons, item)

    end
end

function newgame:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)
    for i, item in pairs(newgame.items) do
        item:update(dt)
    end

    for i, species in pairs(specieschoice.species_icons) do
        species:update(dt)
    end
    hint:update(dt)
end

function newgame:enter(previous) -- run every time the state is entered
    menucam.rot         =   math.rad(newgame.rotation)
    if specieschoice.selected then specieschoice:onClick() end
end

function newgame:draw()
    menucam:attach()
    --Drawing splash image
    love.graphics.draw(background.image, background.pos:unpack())

    for i, item in pairs(newgame.items) do
        item:draw()
    end

    for i, species in pairs(specieschoice.species_icons) do
        species:draw()
    end

    menucam:detach()

    watermark()
    hint:draw()
    if DEBUG then debug_draw_stats(menucam) end

end

function newgame:keyreleased(key)

    if key == "escape" then
        TRANSITION:goTo(mainmenu)
    end

    if key == " " then
        tprint (newgame.items)

    end

end

function newgame:mousereleased(m_x, m_y, button)
    local x, y = menucam:mousepos()
    if button == "l" then
        for i, species in pairs(specieschoice.species_icons) do
            if species.rect:contains(x, y) then
                click_handled   =   species:onClick()
            end
        end

        if not click_handled then
            for i, item in pairs(newgame.items) do
                if item.rect:contains(x, y) then
                    item:onClick()
                end
            end
        end
    elseif button == "r" then
        TRANSITION:goTo( mainmenu )
    end
    --~ if button == "l" then
        --~ if left_arrow:contains(m_x, m_y) then
            --~ preview_alpha = 0
            --~ rb:prev()
        --~ elseif right_arrow:contains(m_x, m_y) then
            --~ preview_alpha = 0
            --~ rb:next()
        --~ elseif play_button.rect:contains(m_x, m_y) then
            --~ seeding.species =   rb:get()
            --~ Gamestate.switch(seeding)
        --~ end
--~
    --~ end

end
