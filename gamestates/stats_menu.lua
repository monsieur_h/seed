------------------------------------------------------------------------
-- SOUND_OPTION - let's choose thoses graphical options
------------------------------------------------------------------------

statmenu              =   Gamestate.new()
statmenu.position     =   vector(1650, -540)
statmenu.rotation     =   0
statmenu.zoom         =   1.5


function statmenu:enter()
    print("Entering stats menu")

    -- Creating list of items

    item_list   =   {"Time played", "Tree killed", "Achievements", "Back"}
    value_list  =   {math.floor(gametime) .. "s", 0, nil, nil}

    line_offset =   (love.graphics.getHeight()/2) / #item_list
    line_offset =   line_offset -   big_font:getHeight()

    menu        =   {}

    ypos  =   love.graphics.getHeight()/2
    -- Building the menu items
    for k,v in pairs(item_list) do
        local item  =   MenuItem( item_list[k], value_list[k])
        local xpos  =   love.graphics.getWidth()/2 - item.rect.width/2
        item:setPos( xpos, ypos )
        ypos = ypos+line_offset

        function item:onClick()
            self:click()
            if item.label=="Achievements" then
                TRANSITION:goTo(achievementsmenu)

            elseif item.label=="Back" then
                TRANSITION:goTo(mainmenu)

            end
        end
        table.insert(menu, item)
    end

end

function statmenu:init()
    --Instanciate options items


end

function statmenu:update(dt)
    menucam:move(math.sin(gametime)/8,math.cos(gametime*0.5)/8)
    TRANSITION:update(dt)
    x, y = love.mouse.getPosition()
    for k, item in pairs(menu) do
        item:update(dt)

        if item:contains(x, y) then
            item:onHover()
        end
    end
end

function statmenu:keyreleased(key, unicode)
    if key == "esc" then
        TRANSITION:goTo(mainmenu)
    end
end

function statmenu:mousereleased(m_x, m_y, button)
    for k, item in pairs(menu) do
        if item:contains(m_x, m_y) then
            item:onClick()
        end
    end
end

function statmenu:draw()
    menucam:attach()
    --Drawing back image
    love.graphics.draw(background.image, background.pos:unpack())
    menucam:detach()

    love.graphics.setFont(big_font)
    for k, item in ipairs(menu) do
        item:draw()

    end

    if DEBUG then debug_draw_stats(menucam) end
    watermark()

end
