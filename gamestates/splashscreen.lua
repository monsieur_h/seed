------------------------------------------------------------------------
-- SPLASHSCREEN displays a header and waits for user input
------------------------------------------------------------------------

splashscreen            =   Gamestate.new()
splashscreen.position   =   vector(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
splashscreen.rotation   =   0
splashscreen.zoom       =   0.4
function splashscreen:init()
    menucam             =   camera(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
    menucam.zoom        =   splashscreen.zoom

    TRANSITION.cam      =   menucam

    background          =   {}
    background.image    =   love.graphics.newImage("res/images/gui/main_menu/background.png")-- Background image
    background.pos      =   vector(love.graphics.getWidth()/2-background.image:getWidth()/2, love.graphics.getHeight()/2-background.image:getHeight()/2)

    title               =   {}
    title.image         =   love.graphics.newImage("res/images/gui/main_menu/label_title.png")-- Seed title
    title.pos           =   vector(love.graphics.getWidth()/2-title.image:getWidth()/2, love.graphics.getHeight()/2-title.image:getHeight()/2)
    title.fadeout       =   false
    title.alpha         =   255
    function title:update(dt)
        if self.fadeout then
            self.alpha  =   self.alpha - 400 * dt
            if self.alpha < 0 then self.alpha = 0 end
        end
    end
    love.audio.play(mainmenu_music)
    TRANSITION.TIME     =   1.0
    TRANSITION.ONGOING  =   false

end

function splashscreen:enter()
    TRANSITION.ONGOING      =   false
    TRANSITION.ELAPSED      =   0

end

function splashscreen:draw()
    menucam:attach()



    --Drawing splash image
    love.graphics.draw(background.image, background.pos:unpack())

    if GAMECONFIG.options["post-process"] == "on" then
        local x, y = love.mouse.getPosition()
        --~ FX:sendMousePos(x, y, "rays" )
        --~ FX:apply("bloom")
    end


    if title.fadeout then
        love.graphics.setColor(255, 255, 255, title.alpha)
    end
    love.graphics.draw(title.image, title.pos:unpack())


    menucam:detach()

    if GAMECONFIG.options["post-process"] == "on" then
        FX:stop()
    end

    --~ love.graphics.printf(_("Press any key to continue..."), 0, love.graphics.getHeight()/5*4, love.graphics.getWidth(), "center")

    if DEBUG then
        debug_draw_stats(menucam)
    end



    watermark()
end

function splashscreen:update(dt)
    TRANSITION:update(dt)
    title:update(dt)
    if TRANSITION.ONGOING then

    end
    menucam:move(math.sin(gametime)/8,math.cos(gametime)/8)

end

function splashscreen:keyreleased(key)
    if key=="escape" then
        love.event.quit()
    elseif key == "d" then
        print("toggling debug")
        -- no code here since it's handled in the main.lua
    elseif key == "p" then
        print("post effect") --ditto
    else
        title.fadeout   =   true
        TRANSITION:goTo(mainmenu)
    end
end

function splashscreen:mousereleased(x, y, button)
    if button=="l" or button=="r" then --No event on mouse wheel
        title.fadeout   =   true
        TRANSITION:goTo(mainmenu)
    end
end
