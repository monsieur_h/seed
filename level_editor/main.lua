camera      =   require "camera"
Gamestate   =   require "gamestate"
ringbuffer  =   require "ringbuffer"
dofile("table.save.lua") --TODO check this for correctness. Thanks

APP_NAME    =   "level_editor"
PROJECT_VERSION     =   "0.0.1"
IMG_NAME    =   nil
IMAGE       =   nil
POLYGON     =   nil
COLLIDABLES =   nil
RESOURCES   =   {}
HUD         =   {}
OPTIONS     =   {}
CURSOR_MODE =   "none"

cam         =   camera(0, 0, 1, 0) -- Camera at origin point, with no zoom
scroll_speed=   10
zoom_speed  =   0.05


testing     =   Gamestate.new() --Testing the level
building    =   Gamestate.new() --Building polys
choosing    =   Gamestate.new() --Choosing a file


function love.load()
    Gamestate.registerEvents()
    print("Project version "..PROJECT_VERSION)
    love.graphics.setCaption(APP_NAME.." "..PROJECT_VERSION)
    love.filesystem.setIdentity(APP_NAME)

    -- Record the gametime
    gametime        =   0

    Gamestate.switch(choosing)

end

function love.draw()
    love.graphics.setColor( 255, 255, 255, 255 )

end
------------------------------------------------------------------------
-- Choosing gamestates: lists all files usable as level, and lets user
-- choose the one to load
------------------------------------------------------------------------
function choosing:init()
    print("Choosing mode")
    if not love.filesystem.isDirectory("source") then
        print("Creating source_image folder")
        love.filesystem.mkdir("source")
    end

    if not love.filesystem.isDirectory("destination") then
        print("Creating destination folder")
        love.filesystem.mkdir("destination")
    end

    l_images    =   list_source_images()
    current_img =   1
    if #l_images==0 then
        print("No images found")
        message     =   "Drop .png files in the source directory!"
        open_data_directory()
    else
        table.insert(l_images, "/open folder")

    end

end

function choosing:update(dt)

    if #l_images == 0 then
        l_images    =   list_source_images()

    end
end

function choosing:draw()
    if #l_images > 0 then
        local i=0
        local offset=25
        for k,v in pairs(l_images) do
            if k == current_img then
                love.graphics.print(">"..v, 25, offset+i*offset)
            else
                love.graphics.print(v, 25, offset+i*offset)
            end
            i=i+1
        end
    else
        love.graphics.print(message, 10, 100)
    end

end

function choosing:keyreleased(key, unicode)
    if key=="up" and current_img > 1 then
        current_img = current_img -1
    end

    if key=="down" and current_img < #l_images then
        current_img = current_img +1
    end

    if key=="escape" then
        love.event.quit()
    end

    if key=="return" and #l_images>0 then
        if current_img == #l_images then --if it's the last one, then open the game folder
            open_data_directory()

        else
            print("Chosen "..l_images[current_img])
            IMAGE   =   l_images[current_img]
            IMG_NAME=   l_images[current_img]
            Gamestate.switch(building)
        end

    end


end

------------------------------------------------------------------------
-- Building gamestate: lets the user build a level by adding collision
-- polygons and such
------------------------------------------------------------------------
function building:init()
    print("Initing building mode")
    IMAGE   =   love.graphics.newImage("source/"..IMAGE)

end

function building:enter()
    --Building HUD....
    HUD =   {}
    if not OPTIONS.mouseGrab then OPTIONS.mouseGrab = false end

    -- Button to lock the cursor
    cursorLockButton        =   {}
    cursorLockButton.text   =   "grab cursor"
    cursorLockButton.x      =   10
    cursorLockButton.y      =   10
    cursorLockButton.width  =   75
    cursorLockButton.height =   50
    cursorLockButton.state  =   "free"
    cursorLockButton.selected=  OPTIONS.mouseGrab
    function cursorLockButton:onClick()
        OPTIONS.mouseGrab   =   not OPTIONS.mouseGrab
        love.mouse.setGrab(OPTIONS.mouseGrab)
        cursorLockButton.selected   =   not cursorLockButton.selected
    end
    table.insert(HUD, cursorLockButton)


    -- Button to add a colidable
    addCollidableButton     =   {}
    addCollidableButton.text=   "Add collision"
    addCollidableButton.x   =   95
    addCollidableButton.y   =   10
    addCollidableButton.width   =   75
    addCollidableButton.height  =   50
    addCollidableButton.selected=   false
    function addCollidableButton:onClick()
        print("add button clicked")
        addWaterSpot.selected   =   false
        if addCollidableButton.selected then
            CURSOR_MODE =   "none"
            addCollidableButton.selected    =   false
        else
            CURSOR_MODE =   "add_point"
            addCollidableButton.selected    =   true
        end

    end
    table.insert(HUD, addCollidableButton)

    -- Button to add a waterspot
    addWaterSpot     =   {}
    addWaterSpot.text=   "Add water source"
    addWaterSpot.x   =   95
    addWaterSpot.y   =   70
    addWaterSpot.width   =   75
    addWaterSpot.height  =   50
    addWaterSpot.selected=   false
    function addWaterSpot:onClick()
        print("add water spot clicked")
        addCollidableButton.selected =  false
        if addWaterSpot.selected then
            CURSOR_MODE =   "none"
            addWaterSpot.selected    =   false
        else
            CURSOR_MODE =   "add_water"
            addWaterSpot.selected    =   true
        end

    end
    table.insert(HUD, addWaterSpot)

    -- Button to switch to test mode
    testLevelButton         =   {}
    testLevelButton.text    =   "Test mode"
    testLevelButton.x       =   190
    testLevelButton.y       =   10
    testLevelButton.width   =   75
    testLevelButton.height  =   50
    testLevelButton.selected=   false
    function testLevelButton:onClick()
        Gamestate.switch(testing)
    end
    table.insert(HUD, testLevelButton)
end

function building:update(dt)
    --TODO: move camera
    if cursorLockButton.state == "free" then
        locked = false
    else
        locked = true
    end
    update_camera(cam, locked)

end

function building:draw()

    cam:attach()
    love.graphics.draw(IMAGE, 0, 0)
    draw_poly()
    draw_water()

    cam:detach()
    draw_hud(HUD)


    if message then
        love.graphics.print(message, 10, 10)
    end

end

function building:keyreleased(key, unicode)
    if key=="escape" then
        -- TODO: switch to quitting mode
    end

    if key=="return" then
        save_level()
    end
end

function building:mousepressed(x, y, button)

    -- Handes zoom
    if button == "wu" then
        cam.zoom    =   cam.zoom + zoom_speed
    end

    if button == "wd" then
        cam.zoom    =   cam.zoom - zoom_speed
    end

    if button == "l" then
        local click_handled =   false
        if #HUD ~= 0 then
            for i,button in pairs(HUD) do
                local yclick = false
                local xclick = false
                if x > button.x and x < button.x+button.width then
                    xclick=true
                end
                --TODO: y
                if y>button.y and y < button.y+button.height then
                    yclick=true
                end

                if yclick and xclick then
                    button:onClick()
                    click_handled   =   true
                end
            end
        end

        if CURSOR_MODE=="add_point" and not click_handled then
            m_x, m_y = cam:mousepos()
            append_to_poly(m_x, m_y)
        elseif CURSOR_MODE=="add_water" and not click_handled then
            m_x, m_y = cam:mousepos()
            add_water(m_x, m_y)
        end
    end

end

------------------------------------------------------------------------
-- Building gamestate: lets the user build a level by adding collision
-- polygons and such
------------------------------------------------------------------------
function testing:init()

end

function testing:enter()
    HUD =   {}
    -- Button to switch to test mode
    testLevelButton         =   {}
    testLevelButton.text    =   "Test mode"
    testLevelButton.x       =   190
    testLevelButton.y       =   10
    testLevelButton.width   =   75
    testLevelButton.height  =   50
    testLevelButton.selected=   true
    function testLevelButton:onClick()
        Gamestate.switch(building)
    end
    table.insert(HUD, testLevelButton)

    OPTIONS.show_poly       =   false

    showPolyButton          =   {}
    showPolyButton.text     =   "Show collidable"
    showPolyButton.x        =   10
    showPolyButton.y        =   10
    showPolyButton.width    =   75
    showPolyButton.height   =   50
    showPolyButton.selected =   false
    function showPolyButton:onClick()
        OPTIONS.show_poly       =   not OPTIONS.show_poly
        showPolyButton.selected =   not showPolyButton.selected
    end
    table.insert(HUD, showPolyButton)


    moreBallsButton         =   {}
    moreBallsButton.text    =   "+10 balls"
    moreBallsButton.x       =   95
    moreBallsButton.y       =   10
    moreBallsButton.width   =   75
    moreBallsButton.height  =   50
    moreBallsButton.selected=   false
    function moreBallsButton:onClick()
        OPTIONS.nb_balls    =   OPTIONS.nb_balls +10
        generate_balls()
    end
    table.insert(HUD, moreBallsButton)

    moreBallsButton         =   {}
    moreBallsButton.text    =   "-10 balls"
    moreBallsButton.x       =   95
    moreBallsButton.y       =   70
    moreBallsButton.width   =   75
    moreBallsButton.height  =   50
    moreBallsButton.selected=   false
    function moreBallsButton:onClick()
        OPTIONS.nb_balls    =   OPTIONS.nb_balls -10
        generate_balls()
    end
    table.insert(HUD, moreBallsButton)
    -- Initing physic engine -> BOX2D \o/
    love.physics.setMeter(64) --the height of a meter our worlds will be 64px
    WORLD       = love.physics.newWorld(0, 8.91*64, true) --create a world for the bodies to exist in with horizontal gravity of 0 and vertical gravity of 9.81
    ball_img    = love.graphics.newImage("love_ball.png")
    BALLGROUP   = {}
    if not OPTIONS.nb_balls then OPTIONS.nb_balls = 50 end
    generate_balls()


    --Creating level collision map
    if POLYGON and #POLYGON >= 4 then
        levelshape  =   love.physics.newChainShape( false, unpack(POLYGON) )
        levelbody   =   love.physics.newBody(WORLD, 0,0, "static")
        levelfixt   =   love.physics.newFixture(levelbody, levelshape)
    end

end

function testing:update(dt)
     --TODO: move camera
    if cursorLockButton.state == "free" then
        locked = false
    else
        locked = true
    end
    update_camera(cam, locked)
    WORLD:update(dt)
end

function testing:draw()
    cam:attach()
    --TODO: draw world
    love.graphics.draw(IMAGE, 0, 0)
    if OPTIONS.show_poly then
        draw_poly()
    end

    for i, v in ipairs(BALLGROUP) do
        love.graphics.draw(v.i, v.b:getX(), v.b:getY(), v.b:getAngle(), 1, 1, v.ox, v.oy)
    end

    cam:detach()
    --TODO: draw hud
    draw_hud(HUD)
end

function testing:mousepressed(x, y, button)

    -- Handes zoom
    if button == "wu" then
        cam.zoom    =   cam.zoom + zoom_speed
    end

    if button == "wd" then
        cam.zoom    =   cam.zoom - zoom_speed
    end

    if button == "l" then
        local click_handled =   false
        if #HUD ~= 0 then
            for i,button in pairs(HUD) do
                local yclick = false
                local xclick = false
                if x > button.x and x < button.x+button.width then
                    xclick=true
                end
                --TODO: y
                if y>button.y and y < button.y+button.height then
                    yclick=true
                end

                if yclick and xclick then
                    button:onClick()
                    click_handled   =   true
                end
            end
        end

        --~ if CURSOR_MODE=="add_point" and not click_handled then
            --~ m_x, m_y = cam:mousepos()
            --~ append_to_poly(m_x, m_y)
--~
        --~ end
    end

end

------------------------------------------------------------------------
-- Utilities functions
------------------------------------------------------------------------
function list_source_images()
    local dir = "/source"
    print("Looking into"..love.filesystem.getSaveDirectory()..dir)
    local files = love.filesystem.enumerate(dir)
    local image_list    =   {}
    for k, file in pairs(files) do
        print(k .. ". " .. file) --outputs something like "1. main.lua"
        if string.find(file, ".png") then
            table.insert(image_list, file)
        end
    end
    return image_list
end

function draw_hud(p_hud)
love.graphics.setLine(1, "smooth")
if #p_hud ~= 0 then
        for i,button in pairs(HUD) do

            if button.selected then
                love.graphics.setColor( 75, 255, 75, 155 )
            else
                love.graphics.setColor( 255, 255, 255, 255 )
            end
            love.graphics.rectangle( "line", button.x, button.y, button.width, button.height )
            love.graphics.printf(button.text, button.x, button.y+button.height/3, button.width, "center")
        end
    end
end

function draw_poly()
    if POLYGON then
        if taille < 2 then
            love.graphics.circle("fill", POLYGON[1], POLYGON[2], 5)
        else
            love.graphics.setLine(5, "smooth")
            love.graphics.line(POLYGON)
        end
    end
end

function append_to_poly(p_x, p_y)
    if not POLYGON then
        POLYGON =   {}
        taille  = 0
    end
    taille  =   taille + 1
    table.insert(POLYGON, p_x)
    table.insert(POLYGON, p_y)
end

function prepend_to_poly(p_x, p_y)
    if not POLYGON then
        POLYGON =   {}
        taille  = 0
    end
    taille  =   taille + 1
    table.insert(POLYGON, 0, p_y)
    table.insert(POLYGON, 0, p_x)
end

function generate_balls()
    if OPTIONS.nb_balls <= 0 then OPTIONS.nb_balls=1 end
    print("Generating "..OPTIONS.nb_balls.." balls...")
    for k, v in pairs(BALLGROUP) do
        v.f:destroy()
        v.b:destroy()
        v = nil
    end
    --~ BALLGROUP   =   nil
    collectgarbage()
    print("Ballgroup size: "..#BALLGROUP)

    BALLGROUP   =   {}
    for i = 1 , OPTIONS.nb_balls do
        local x, y = math.random(0, IMAGE:getWidth()), -math.random(-300, 100)
        local t = {}
        t.b  = love.physics.newBody(WORLD, x, y, "dynamic")
        t.s  = love.physics.newCircleShape(32)
        t.f  = love.physics.newFixture(t.b, t.s)
        t.i  = ball_img
        t.ox = 32
        t.oy = 32
        table.insert(BALLGROUP, t)
    end
end

function add_water(p_x, p_y)
    if not RESOURCES then RESOURCES = {} end
    if not RESOURCES.water then RESOURCES.water = {} end
    local waterspot = {}
    waterspot.x =   p_x
    waterspot.y =   p_y
    table.insert(RESOURCES.water, waterspot)

end

function draw_water()
    if RESOURCES.water then
        love.graphics.setColor( 0, 0, 255)
        for k, v in pairs(RESOURCES.water) do
            love.graphics.circle( "fill", v.x, v.y, 32 )

        end
    end

end
-- Opens the data directory with the system's file browser to put files in
function open_data_directory()
    local dir   =   love.filesystem.getSaveDirectory()

    local homepath    =   os.getenv("HOME")
    if string.find(homepath, "/home") then
        linux   =   true
    end

    if linux then
        print("User is running UNIX-like system")
        os.execute("nautilus "..dir)
    else
        print("User is running Windows system")
        os.execute("explorer "..dir)
    end
    print("Opening "..dir.."...")
end

function update_camera(p_cam, locked)
    -- TODO: make border optionnal

    -- Going up?
    if love.keyboard.isDown("up") then
        p_cam:move(0, -scroll_speed)
    end
    if love.mouse.getY() == 0 and OPTIONS.mouseGrab then
        p_cam:move(0, -scroll_speed)
    end

    --Doing down?
    if love.keyboard.isDown("down") then
        p_cam:move(0, scroll_speed)
    end
    if love.mouse.getY() == love.graphics.getHeight() -1 and OPTIONS.mouseGrab then
        p_cam:move(0, scroll_speed)
    end

    -- Going left?
    if love.keyboard.isDown("left") then
        p_cam:move(-scroll_speed, 0)
    end
    if love.mouse.getX() == 0 and OPTIONS.mouseGrab then
        p_cam:move(-scroll_speed, 0)
    end

    --Going right?
    if love.keyboard.isDown("right") then
        p_cam:move(scroll_speed, 0)
    end

    if love.mouse.getX() == love.graphics.getWidth() -1 and OPTIONS.mouseGrab then
        p_cam:move(scroll_speed, 0)
    end

end

function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    else
      print(tostring(formatting) .. tostring(v))
    end
  end
end

function save_level(name)
    print("Saving level")
    if not name then name="level_0" end
    tprint(POLYGON)
    tprint(RESOURCES)
    local dir   =   love.filesystem.getSaveDirectory()
    filename    =   dir.."/destination/"..name
    print("Final filename is "..filename)

    level   =   {}
    level.collidable=   POLYGON
    level.resources =   RESOURCES
    level.imagename =   IMG_NAME
    table.save(level, filename)
    --~ open_data_directory()

end
