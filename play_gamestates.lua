------------------------------------------------------------------------
-- @file play_gamestates.lua
-- File storing the differents gamestates of ingame modes and their callbacks
------------------------------------------------------------------------

seeding =   Gamestate.new()
growing =   Gamestate.new()

--Drawing groups
GAME_OBJECTS    =   {}
PARTICLES       =   {}
ENV_OBJECTS     =   {}
GUI             =   {}
SURFACE_OBJECT  =   {}
world           =   {}


--Transition management
seeding.zoom    =   1
seeding.position=   vector(0, 0)
seeding.rotation=   0
seeding.zoom    =   1
growing.zoom    =   0.5
seedcam             =   camera(0, 0, 1, 0) -- Camera at origin point, with no zoom
seedcam.u_zoom      =   {min=0.30, max=1, wanted=0.5}
function seedcam:update(dt, x, y)
    local mx, my    =   0, 0  --Value to move the camera in the end
    TRANSITION.TIME =   0.5
    -- Setting zoom to the point needed
    local current_zoom = math.floor(self.zoom * 100)/100
    --~ self.u_zoom.wanted = math.floor(self.u_zoom.wanted * 100)/100
    --~ self.u_zoom.wanted = clamp(self.u_zoom.wanted, self.u_zoom.max, self.u_zoom.min)
    if self.u_zoom.wanted ~= current_zoom then
        self.u_zoom.wanted  =   math.floor(self.u_zoom.wanted * 100 ) / 100
        self.u_zoom.wanted  =   clamp(self.u_zoom.wanted, self.u_zoom.min, self.u_zoom.max)

        if self.u_zoom.wanted > current_zoom then
            self.zoom = self.zoom + 0.02
        elseif self.u_zoom.wanted < current_zoom then
            self.zoom = self.zoom - 0.02
        end
        self.zoom   =   math.floor(self.zoom * 100)/100

    end

    -- Scroll management (when mouse hits the borders) or arrows used
    if m_x == 0 or love.keyboard.isDown("left") then
        mx  =   -GAMECONFIG.options["scroll_speed"]
    end
    if m_x == love.graphics.getWidth() -1 or love.keyboard.isDown("right") then
        mx  =   GAMECONFIG.options["scroll_speed"]
    end
    if m_y == 0 or love.keyboard.isDown("up") then
        my  =   -GAMECONFIG.options["scroll_speed"]
    end
    if m_y == love.graphics.getHeight() -1 or love.keyboard.isDown("down") then
        my  =   GAMECONFIG.options["scroll_speed"]
    end

    -- Prevent from moving the camera out of the level
    self:safeMove( mx, my )
end

function seedcam:safeMove( p_x, p_y )
    if self.level then
        topleft     =   {}
        bottomright =   {}
        topleft.x, topleft.y            =   seedcam:worldCoords(0, 0)
        bottomright.x, bottomright.y    =   seedcam:worldCoords(love.graphics.getWidth(), love.graphics.getHeight() )
        if topleft.x + p_x <= self.level.pos.x and p_x<0 then
            p_x =   self.level.pos.x - topleft.x
        end

        if bottomright.x + p_x >= ( self.level.pos.x + self.level.width )   and p_x>0 then
            p_x =   self.level.pos.x - bottomright.x + self.level.width
        end

        if bottomright.y + p_y >= ( self.level.pos.y + self.level.height )  and p_y>0 then
            p_y =   self.level.pos.y - bottomright.y + self.level.height
        end
    end
    self:move( p_x, p_y )
end

function seedcam:bindLevel( p_level )
    self.level  =   p_level
end

function seedcam:wheel(button)
    if button=="wu" then
        self.u_zoom.wanted  =   self.u_zoom.wanted + 0.1
    elseif button=="wd" then
        self.u_zoom.wanted  =   self.u_zoom.wanted - 0.1
    end
end

------------------------------------------------------------------------
-- SEEDING
------------------------------------------------------------------------
function seeding:init()
    self.alpha          =   0
    TRANSITION.TIME     =   0.5     --Redifining the transition duration for this state

    -- Physics
    love.physics.setMeter(64)
    world = love.physics.newWorld(0, 7.91*64, true)

    -- Init level
    current_level   =   Level("data/levels/level_0.level", world)
    seedcam:bindLevel( current_level )
    table.insert(ENV_OBJECTS, current_level)

    --Setting SkyManager
    sky                 =   SkyManager( love.graphics.newImage("res/images/level/sky.png"), current_level )
    sky:setCam( seedcam )

    -- Initing our Seed
    --TODO:refactor this
    species         =   seeding.species
    local xseedpos  =   math.random( -current_level.width/4, current_level.width/4 )
    seed            =   Seed( current_level.width/2 + xseedpos, -200 ) --Centered @mid-level +- 25%
    seed.species    =   species
    seed:setImage(species.seed_image)
    seed:setJumpLeft(species.max_jumps)
    seed:setShape(species.seed_shape)
    seed:setWorld(world)
    seed:setEffectDuration(TRANSITION.TIME)
    table.insert(GAME_OBJECTS, seed)

    --Camera looking at the seed to begin
    seed:update(0)
    seedcam:lookAt( seed.body:getWorldCenter() )


    --~ Register particles effects
    table.insert(PARTICLES, seed.jump_effect)
    table.insert(PARTICLES, seed.creation_effect)


    -- TODO: make it the borders of the level
    screenright         =   love.graphics.getHeight()
    screenright         =   love.graphics.getWidth()
    -- TODO: make the level editor generate borders dynamically
    border_rectangle    =   love.physics.newRectangleShape(10, current_level.image:getHeight() )

    border_left         =   {}
    border_left.body    =   love.physics.newBody(world, 0, current_level.image:getHeight()/2, "static") --Anchor point
    border_left.fixture =   love.physics.newFixture(border_left.body, border_rectangle)

    border_right        =   {}
    border_right.body   =   love.physics.newBody(world,current_level.image:getWidth(), current_level.image:getHeight()/2, "static") --Anchor point
    border_right.fixture=   love.physics.newFixture(border_right.body, border_rectangle)


end

function seeding:enter()
    love.mouse.setGrab(true)
    self.fadingIn   =   true
    self.alpha      =   0 --starting with black
    self.firstFrame =   true
end

function seeding:draw()
    sky:draw( self.alpha )

    --Drawing _moving_ world
    seedcam:attach()
    for idx,obj in ipairs(ENV_OBJECTS) do
        obj:draw( self.alpha )
    end

    sky:drawSunrays()

    for idx,obj in ipairs(GAME_OBJECTS) do
        obj:draw( self.alpha )
    end

    for idx,p in ipairs(PARTICLES) do
        love.graphics.draw(p, 0, 0)
    end
    sky:drawClouds( self.alpha )
    -- INFO: keep that as an example: don't fall into that bug again:
    -- body: getPosition returns the FRACKING CENTER of the shape, not it's DARN TOPLEFT CORNER
    --~ x, y = border_left.body:getWorldCenter()
    --~ love.graphics.rectangle("fill", x, y - current_level.image:getHeight()/2, 10, current_level.image:getHeight() )

    --~ x, y = border_right.body:getPosition()
    --~ love.graphics.rectangle("fill", x, y - current_level.image:getHeight()/2, 10, current_level.image:getHeight() )

    --~ DEBUG
    if DEBUG then
        debug_draw_shapes(GAME_OBJECTS)

    end

    --Drawing _fixed_ HUD
    seedcam:detach()

    watermark()
    for i, gui in pairs(GUI) do
        gui:draw()
    end
    if DEBUG then
        debug_draw_stats(seedcam)
    end
    love.graphics.print( _("You have ")..seed.jumps.._(" jumps remaining."), 10, 10 )

end

function seeding:update(dt)
    if self.fadingIn then
        if self.firstFrame then
            self.firstFrame = false
            dt  =   0
        else
            self.alpha = self.alpha + dt / TRANSITION.TIME * 255
            if self.alpha >= 255 then
                self.fadingIn = false
                self.alpha = 255
                print("fading in is over")
            end
        end
    end
    --~
    --~ local point =   {}
    --~ point.x, point.y=seed.x_pos, seed.y_pos
    --~ track_point(seedcam, point)

    sky:update(dt)
    m_x, m_y    =   love.mouse.getPosition()
    world:update(dt) --Physic world

    for idx,obj in ipairs(ENV_OBJECTS) do
        obj:update(dt)
    end

    --Player parts
    for idx,obj in ipairs(GAME_OBJECTS) do
        obj:update(dt)
    end

    --Particle effects
    for idx,p in ipairs(PARTICLES) do
        p:update(dt)
    end

    for idx,gui in ipairs(GUI) do
        gui:update(dt)
        if not gui:isAlive() then
            table.remove(GUI, idx)
        elseif gui:testPoint(m_x, m_y) then
            gui:onHover(m_x, m_y)
        end
    end

    if seed.planted then
        if not zoom_factor then
            zoom_factor =   (growing.zoom - seeding.zoom)/TRANSITION.TIME
        end
        seedcam.zoom    =   seedcam.zoom + zoom_factor*dt
    end

    if seed:isPlaced() then
        Gamestate.switch(growing)
    end

end

function seeding:keypressed(key, unicode)

    if key == "escape" then
        love.event.quit()
    end

    --~ if key == "e" then
        --~ self.firstFrame = false
    --~ end

    if key == " " then --spacebar pressed
        print("planting!")
        seed:plant()
    end

    --~ if key == "right" then
        --~ seed:jump("right")
    --~ end
--~
    --~ if key == "left" then
        --~ seed:jump("left")
    --~ end

end

function seeding:mousereleased(m_x, m_y, button)
    x,y =   seedcam:mousepos()
    click_handled   =   false

    for i,gui in ipairs(GUI) do
        if gui:onClick(m_x,m_y) then
            click_handled   =   true
        end
    end
    if not click_handled then
        if seed.fixture:testPoint(x, y) then
            radial    =   RadialMenu(seed)
            radial:setItems(seed:getActions())
            radial:setPosition(m_x,m_y)
            table.insert(GUI, radial)
            radial    =   nil
            --~ seed:plant()
        --~ else
            --~ if x<seed.x_pos then
                --~ seed:jump("left")
            --~ else
                --~ seed:jump("right")
            --~ end
        end
    end
end

------------------------------------------------------------------------
-- GROWING
------------------------------------------------------------------------
function growing:init()
    player  =   Tree(species)
    player:setCore(seed)
    player:setCamera(seedcam)

    nut_bar = ResourceBar()
    nut_bar:listenTo(player.resources.nutriment)
    local x, y = 0, 0
    x = 25
    y = love.graphics.getHeight() - 40
    nut_bar:setPos(x, y)
    nut_bar:setName("nutriment")
    nut_bar.color.r, nut_bar.color.g, nut_bar.color.b   =   255, 80, 80


    water_bar = ResourceBar()
    water_bar:listenTo(player.resources.water)
    local x, y = 0, 0
    x = 50
    y = love.graphics.getHeight() - 40
    water_bar:setPos(x, y)
    water_bar:setName("water")
    water_bar.color.r, water_bar.color.g, water_bar.color.b   =   80, 80, 255

    light_bar = ResourceBar()
    light_bar:listenTo(player.resources.light)
    local x, y = 0, 0
    x = 75
    y = love.graphics.getHeight() - 40
    light_bar:setPos(x, y)
    light_bar:setName("light")
    light_bar.color.r, light_bar.color.g, light_bar.color.b   =   255, 200, 0

    table.insert(GUI, light_bar)
    table.insert(GUI, water_bar)
    table.insert(GUI, nut_bar)
end

function growing:enter()
    print("Entering growing mode...")
    love.mouse.setGrab(true)
    seedcam.zoom    =   growing.zoom
    hint:setInterval(60)
end

function growing:update(dt)
    world:update(dt)
    sky:update(dt)
    hint:update(dt)
    --~ print( hint:getTimeSinceLastMessage() )
    m_x, m_y    =   love.mouse.getPosition()

    if love.mouse.isDown( "l" ) then
        if m_previous then
            local x,y   =   seedcam:worldCoords( m_previous.x, m_previous.y )
            local x2, y2=   seedcam:worldCoords( m_x, m_y )
            seedcam:safeMove( x-x2, y-y2 )
        else
            m_previous = {}
        end

        m_previous.x, m_previous.y = m_x, m_y
    else
        m_previous = nil
        seedcam:update(dt, m_x, m_y)
    end

    for idx,obj in ipairs(ENV_OBJECTS) do
        obj:update(dt)
        if obj.dead then
            table.remove(ENV_OBJECTS, idx)
        end
    end

    player:update(dt)
    for idx,obj in ipairs(GAME_OBJECTS) do
        obj:update(dt)
        if obj.dead then
            table.remove(GAME_OBJECTS, idx)
        end
    end

    -- Passing resources arrays to the ghost element
    if player.appending then
        player.appending_element.nutriments =   current_level.nutriments
    end


    for idx,p in ipairs(PARTICLES) do
        p:update(dt)
    end

    --Updating GUI elements
    for idx,gui in ipairs(GUI) do
        gui:update(dt)
        if not gui:isAlive() then
            table.remove(GUI, idx)
        elseif gui:testPoint(m_x, m_y) then
            gui:onHover(m_x, m_y)
        end
    end

end

function growing:draw()
    sky:draw()

    seedcam:attach()
    --Drawing _moving_ world
    for idx,obj in ipairs(ENV_OBJECTS) do
        obj:draw()
    end

    sky:drawSunrays()

    player:draw()

    sky:drawClouds()

    for idx,obj in ipairs(GAME_OBJECTS) do
        obj:draw()
    end

    for idx,p in ipairs(PARTICLES) do
        love.graphics.draw(p, 0, 0)
    end

    seedcam:detach()

    for i, gui in pairs(GUI) do
        gui:draw()
    end

    watermark()

    hint:draw()

    if DEBUG then
        debug_draw_stats(seedcam)
        seedcam:attach()
        local x1,y1 =   seedcam:worldCoords( love.graphics.getWidth(), 0 )
        local x2,y2 =   seedcam:worldCoords( love.graphics.getWidth(), love.graphics.getHeight() )
        love.graphics.setLine( 10 )
        love.graphics.line(x1, y1, x2, y2 )
        seedcam:attach()
    end

    --DRAWING STATIC HUD
    --nope
end

function growing:mousereleased(m_x, m_y, button)
    x,y =   seedcam:mousepos()

    -- If left button is clicked
    if button == "l" then
        click_handled   =   false
        --Check if a gui element is clicked
        for i,gui in ipairs(GUI) do
            if not click_handled then
                if not click_handled then
                    if gui:onClick(m_x,m_y) then
                        click_handled   =   true
                    end
                end
            end
        end

        --If clicking on any player element
        if not click_handled then

            -- If the player is aready appending something
            if player.appending then
                print("Appending something...")
                if element then element.selected = false  end
                player.appending_element:confirm()
                player:connectNutriment(current_level.nutriments)
                player.appending_element=   nil
                player.appending        =   false

            else
                -- If the player is asking for a new append
                if element then element.selected = false  end
                element =   player:testPoint(x, y)
                if element then
                    radial  =   RadialMenu(element)
                    radial:setItems(element:getActions())
                    radial:setPosition(m_x, m_y)
                    table.insert(GUI, radial)
                    radial  =   nil
                end
            end
        end
    end

    -- If mouse is right clicked
    if button == "r" then
        if element then element.selected = false  end
        if player.appending_element then
            print("Cancelling element")
            player:cancel()
        end
    end

    if button == "wu" or button == "wd" then
        seedcam:wheel(button)
    end

    m_pressed   =   nil
end

function growing:keypressed(key, unicode)
    if key == "escape" then
        love.event.quit()
    end

    if key == "t" then
        if player.transparent then
            player:setTransparent(false)
        else
            player:setTransparent(true)
        end
    end

    if key == "r" then
        player.resources.nutriment.current = player.resources.nutriment.max
        player.resources.water.current = player.resources.water.max
        player.resources.light.current = player.resources.light.max
    end

    if key == "e" then
        if sky.timefactor == 0.5 then
            sky.timefactor  =   0.02
        else
            sky.timefactor  =   0.5
        end
    end

end

function growing:mousepressed(p_x, p_y, button)
    if button == "l" then
        m_pressed   = {}
        m_pressed.x = p_x
        m_pressed.y = p_y
    end
end
