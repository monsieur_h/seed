------------------------------------------------------------------------
-- @file resource_loader.lua
-- Utility file to load resources (images, species, levels...)
------------------------------------------------------------------------


------------------------------------------------------------------------
-- Loads data from the species folder and the resource folders
-- @return array of Species object. Images and all attributes are set
------------------------------------------------------------------------
function load_species()
    print("Loading species...")
    local datadir =   "data/species/"
    local ok, chunk, result
    specie_list =   {}

    --assuming that our path is full of lovely files (it should at least contain main.lua in this case)
    local files = love.filesystem.enumerate(datadir)
    for k, file in ipairs(files) do
        print("Found:"..datadir..file)
        s_data  =   love.filesystem.load(datadir..file)
        ok, result = pcall(s_data) -- execute the chunk safely
        if ok then
            
            -- loading list of branch images
            --~ result.branch_image         =   love.graphics.newImage("res/images/sprites/"..result.branch_image)
            if #result.branch_image ~= 0 then
				local tmp	=	{}
				for i, v in pairs( result.branch_image ) do
					tmp[i]	=	love.graphics.newImage( "res/images/sprites/"..result.branch_image[i] )
				end
				result.branch_image	=	tmp
				tmp	=	nil
			end
            result.seed_image           =   love.graphics.newImage("res/images/sprites/"..result.seed_image)
            result.root_image           =   love.graphics.newImage("res/images/sprites/"..result.root_image)
            result.leaf_image           =   love.graphics.newImage("res/images/sprites/"..result.leaf_image)
            result.branch_placeholder   =   love.graphics.newImage("res/images/sprites/"..result.branch_placeholder)
            result.branch_image_empty   =   love.graphics.newImage("res/images/sprites/"..result.branch_image_empty)
            result.root_placeholder     =   love.graphics.newImage("res/images/sprites/"..result.root_placeholder)
            result.leaf_placeholder     =   love.graphics.newImage("res/images/sprites/"..result.leaf_placeholder)
            result.preview_image        =   love.graphics.newImage("res/images/sprites/"..result.preview_image)
            result.preview_icon         =   love.graphics.newImage("res/images/sprites/"..result.preview_icon)
            table.insert(specie_list, result)
        else
            print("Error reading "..file.." : "..tostring(result))
        end
    end
    return specie_list


end
